package main

import (
	"gitlab.com/gitlab-com/gl-infra/pmv/cmd"
)

func main() {
	cmd.Execute()
}
