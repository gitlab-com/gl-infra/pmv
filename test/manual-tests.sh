#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

#########################################
# This script runs a set of manual tests
# which require human interaction.
#########################################

cd "$(dirname "${BASH_SOURCE[0]}")/.."

main() {
  go build .

  run_pmv capture aws -T 'MyTestAWSCredential' --debug --delete-tagged-items

  run_pmv env 'MyTestAWSCredential' --debug >/dev/null

  run_pmv capture gitlab --tags "MyTestGitLabCredential" --delete-tagged-items

  run_pmv env 'MyTestGitLabCredential' --debug >/dev/null
}

run_pmv() {
  echo "--------------------------------------------------"

  set -x
  ./pmv "$@"
  set +x
}

main
