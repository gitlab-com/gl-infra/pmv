package model

import (
	"fmt"
)

func (c *OPItem) ShareLink(accountID string) string {
	return fmt.Sprintf(
		"https://start.1password.com/open/i?a=%s&v=%s&i=%s&h=%s",
		accountID,
		c.Vault.ID,
		c.ID,
		"gitlab.1password.com",
	)
}
