package model

// OPItem represents an Item.
type OPItem struct {
	ID       string     `json:"id,omitempty"`
	Vault    *OPVault   `json:"vault,omitempty"`
	Title    string     `json:"title"`
	Category string     `json:"category"`
	Fields   []*OPField `json:"fields"`
	Tags     []string   `json:"tags"`
	URLs     []*OPURL   `json:"urls"`
}

// OPVault represents a Vault.
type OPVault struct {
	ID string `json:"id"`
}

// OPField represents a Field.
type OPField struct {
	ID    string `json:"id"`
	Type  string `json:"type"`
	Label string `json:"label,omitempty"`
	Value string `json:"value,omitempty"`
}

// OPURL represents a URL.
type OPURL struct {
	Primary bool   `json:"primary"`
	HRef    string `json:"href"`
}

// OPWhoAmI represents the response from op whoami --format=json.
type OPWhoAmI struct {
	URL         string `json:"url"`
	Email       string `json:"email"`
	UserUUID    string `json:"user_uuid"`
	AccountUUID string `json:"account_uuid"`
}
