package model

import (
	"encoding/json"
	"fmt"
)

// ParseItem will parse an item from JSON.
func ParseItem(jsonItem string) (*OPItem, error) {
	item := &OPItem{}

	err := json.Unmarshal([]byte(jsonItem), &item)
	if err != nil {
		return nil, fmt.Errorf("unmarshal failed: %w", err)
	}

	return item, nil
}
