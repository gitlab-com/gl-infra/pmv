package tokenvalidator

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/gitlab"
)

func gitlabValidator(envMap map[string]string) (bool, error) {
	token, ok := envMap["GITLAB_TOKEN"]
	if !ok {
		return false, nil
	}

	gitlabInstance, ok := envMap["GITLAB_INSTANCE"]
	if !ok {
		gitlabInstance = "https://gitlab.com"
	}

	username, err := gitlab.VerifyToken(gitlabInstance, token)
	if err != nil {
		return true, fmt.Errorf("failed to verify gitlab token: %w", err)
	}

	log.Info().
		Str("instance", gitlabInstance).
		Str("username", username).
		Msg("🦊 GitLab token verified")

	return true, nil
}
