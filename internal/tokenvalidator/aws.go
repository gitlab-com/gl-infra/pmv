package tokenvalidator

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/aws"
)

func awsValidator(envMap map[string]string) (bool, error) {
	accessKey, ok := envMap["AWS_ACCESS_KEY"]
	if !ok {
		return false, nil
	}

	secret := envMap["AWS_SECRET_ACCESS_KEY"]
	sessionToken := envMap["AWS_SESSION_TOKEN"]

	username, awsAccountID, accountAlias, err := aws.VerifyCallerIdentity(accessKey, secret, sessionToken)
	if err != nil {
		return true, fmt.Errorf("failed to verify aws token: %w", err)
	}

	log.Info().
		Str("username", username).
		Str("aws_account_id", awsAccountID).
		Str("alias", accountAlias).
		Msg("☁️ AWS Token validated")

	return true, nil
}
