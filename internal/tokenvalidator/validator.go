package tokenvalidator

import "fmt"

type validatorFunc func(envMap map[string]string) (bool, error)

// Add additional validators here...
var validators = []validatorFunc{
	awsValidator,
	gitlabValidator,
}

func ValidateToken(envMap map[string]string) error {
	for _, v := range validators {
		matched, err := v(envMap)
		if err != nil {
			return fmt.Errorf("failed to validate token: %w", err)
		}

		if matched {
			return nil
		}
	}

	// No matches
	return nil
}
