package errors

import (
	"errors"
)

// ErrSetup indicates that an error may require the user to take action and fix their setup.
var ErrSetup = errors.New("pmv is unable to continue due to an environment setup error")
