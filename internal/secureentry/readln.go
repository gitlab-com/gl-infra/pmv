package secureentry

import (
	"fmt"
	"strings"
	"syscall"

	"golang.org/x/term"
)

// SecureRead will read a secure value from a user. In future, we may want to support alternative input methods, such as
// pinentry.
func SecureRead() (string, error) {
	secureValueBytes, err := term.ReadPassword(syscall.Stdin)
	if err != nil {
		return "", fmt.Errorf("failed to read secure input: %w", err)
	}

	// Always write a newline
	fmt.Println("")

	secureValue := strings.TrimSpace(string(secureValueBytes))

	return secureValue, nil
}
