package utils

import (
	"io"

	"github.com/rs/zerolog/log"
)

// DeferWrapClose will provide a wrapper for an io.Closer with logging if the close returns an error.
func DeferWrapClose(closer io.Closer) func() {
	return func() {
		if closer == nil {
			return
		}

		err := closer.Close()
		if err == nil {
			return
		}

		log.Warn().
			Err(err).
			Msg("unable to close body")
	}
}
