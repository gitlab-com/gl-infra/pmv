package ui

import (
	"os"

	"github.com/charmbracelet/huh/spinner"
)

// Spin produces a temporary spinner while waiting for something to complete.
func Spin[E any](title string, f func() (E, error)) (E, error) {
	var (
		result E
		err    error
	)

	_ = spinner.New().Output(os.Stderr).Title(title).Action(func() {
		result, err = f()
	}).Run()

	return result, err
}
