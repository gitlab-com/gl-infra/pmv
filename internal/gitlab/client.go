package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/utils"
)

func makeGitLabRequest(method string, gitLabInstance string, path string, params map[string]string, gitLabToken string, response any) error {
	urlForInstance, err := getURLForInstance(gitLabInstance, path, params)
	if err != nil {
		return err
	}

	client := http.Client{
		Timeout: time.Second * 2, // Timeout after 2 seconds
	}

	req, err := http.NewRequestWithContext(context.Background(), method, urlForInstance, nil)
	if err != nil {
		return fmt.Errorf("create http request failed: %w", err)
	}

	req.Header.Set("User-Agent", "pmv")
	req.Header.Set("PRIVATE-TOKEN", gitLabToken) // nolint:canonicalheader

	res, err := client.Do(req) // nolint:bodyclose
	if err != nil {
		return fmt.Errorf("http request failed: %w", err)
	}

	defer utils.DeferWrapClose(res.Body)

	if res.StatusCode >= http.StatusBadRequest {
		return fmt.Errorf("http status code %d: %w", res.StatusCode, errBadStatus)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("http read failed: %w", err)
	}

	jsonErr := json.Unmarshal(body, response)
	if jsonErr != nil {
		return fmt.Errorf("unmarshall failed: %w", err)
	}

	return nil
}

func getURLForInstance(gitLabInstance string, path string, params map[string]string) (string, error) {
	u, err := url.Parse(gitLabInstance)
	if err != nil {
		return "", fmt.Errorf("unable to parse url %s: %w", gitLabInstance, err)
	}

	u.Path = "/api/v4/" + path

	q := u.Query()

	for k, v := range params {
		q.Add(k, v)
	}

	u.RawQuery = q.Encode()

	return u.String(), nil
}
