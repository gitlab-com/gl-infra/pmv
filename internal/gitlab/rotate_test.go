package gitlab

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestNormalizeExpiry(t *testing.T) {
	testCases := []struct {
		name     string
		input    string
		expected string
	}{
		{
			name:     "Empty input",
			input:    "",
			expected: "2025-01-01",
		},
		{
			name:     "Valid duration",
			input:    "24h",
			expected: "2024-01-03",
		},
		{
			name:     "Invalid duration",
			input:    "invalid",
			expected: "invalid",
		},
		{
			name:     "Valid date",
			input:    "2023-05-01",
			expected: "2023-05-01",
		},
	}

	t.Parallel()

	now := time.Date(2024, 01, 02, 0, 0, 0, 0, time.UTC)

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			result := normalizeExpiry(now, tc.input)
			require.Equal(t, tc.expected, result, "Unexpected value")
		})
	}
}
