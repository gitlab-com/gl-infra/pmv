package gitlab

import (
	"errors"
	"fmt"
	"net/http"
)

type gitlabUser struct {
	Username string `json:"username"`
}

var errBadStatus = errors.New("bad http response code")

// VerifyToken will verify the caller identity given creds,
// and return some useful information about the account.
func VerifyToken(gitLabInstance string, gitLabToken string) (username string, err error) {
	u := gitlabUser{}

	err = makeGitLabRequest(http.MethodGet, gitLabInstance, "user", nil, gitLabToken, &u)
	if err != nil {
		return "", fmt.Errorf("gitlab api call failed: %w", err)
	}

	return u.Username, nil
}
