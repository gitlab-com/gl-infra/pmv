package gitlab

import (
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"time"
)

type gitlabToken struct {
	ID        int64  `json:"id"`
	ExpiresAt string `json:"expires_at"`
	Token     string `json:"token"`
}

var projectBotUserRegexp = regexp.MustCompile(`^project_\d+_bot_`)
var groupBotUserRegexp = regexp.MustCompile(`^group_\d+_bot_`)

var errRotationFailed = errors.New("gitlab token rotation failed")

// RotatePersonalAccessToken uses the GitLab API to exchange a token for a new one.
func RotatePersonalAccessToken(gitLabInstance string, expiresAt string, toRotate, writeToken string) (string, string, error) {
	username, err := VerifyToken(gitLabInstance, toRotate)
	if err != nil {
		return "", "", fmt.Errorf("token is unusable: %w", err)
	}

	if projectBotUserRegexp.MatchString(username) {
		return "", "", fmt.Errorf("project token rotation is currently unsupported. "+
			"Requires https://gitlab.com/gitlab-org/gitlab/-/issues/462419: %w", errRotationFailed)
	}

	if groupBotUserRegexp.MatchString(username) {
		return "", "", fmt.Errorf("group token rotation is currently unsupported. "+
			"Requires https://gitlab.com/gitlab-org/gitlab/-/issues/462419: %w", errRotationFailed)
	}

	currentToken := gitlabToken{}

	err = makeGitLabRequest(http.MethodGet, gitLabInstance, "personal_access_tokens/self", nil, toRotate, &currentToken)
	if err != nil {
		return "", "", fmt.Errorf("can't identify current token. gitlab api call failed: %w", err)
	}

	u := gitlabToken{}

	expiresAt = normalizeExpiry(time.Now(), expiresAt)

	err = makeGitLabRequest(http.MethodPost, gitLabInstance,
		fmt.Sprintf("personal_access_tokens/%d/rotate", currentToken.ID),
		map[string]string{"expires_at": expiresAt}, writeToken, &u)
	if err != nil {
		return "", "", fmt.Errorf("gitlab api call failed: %w", err)
	}

	return u.Token, u.ExpiresAt, nil
}

func normalizeExpiry(now time.Time, expiresAt string) string {
	// By default expire in 1 day less than a year
	if expiresAt == "" {
		e := now.AddDate(1, 0, -1)
		return e.Format("2006-01-02")
	}

	// Try treat the expiry as a duration
	d, err := time.ParseDuration(expiresAt)
	if err == nil {
		e := now.Add(d)
		return e.Format("2006-01-02")
	}

	return expiresAt
}
