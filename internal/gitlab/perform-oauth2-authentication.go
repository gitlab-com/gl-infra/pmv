package gitlab

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	utils "gitlab.com/gitlab-com/gl-infra/pmv/internal/utils"

	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
	gitlaboauth "golang.org/x/oauth2/gitlab"

	pmvoauth2 "gitlab.com/gitlab-com/gl-infra/pmv/internal/oauth2"
)

// This client is defined in https://gitlab.com/groups/gitlab-com/gl-infra/-/settings/applications/291973
const pmvClientID = "6aed31ec07a956476a76b28d6c31999e3ac3244ac274ae74d264ed2a666f4cc9" // gitleaks:allow

// PerformOAuth2Authentication uses Authorization code with Proof Key for Code Exchange (PKCE) to perform
// a secret-less OAuth2 authentication.
// See https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-with-proof-key-for-code-exchange-pkce
// for more details.
func PerformOAuth2Authentication(ctx context.Context) (*oauth2.Token, error) {
	config := &oauth2.Config{
		ClientID: pmvClientID,
		Endpoint: gitlaboauth.Endpoint,
	}

	existing := attemptTokenRefresh(ctx, config)
	if existing != nil {
		log.Info().
			Msg("♻️  Reusing existing token...")

		return existing, nil
	}

	token, err := pmvoauth2.PerformOAuthExchange(ctx, config)
	if err != nil {
		return nil, fmt.Errorf("unable to perform oauth2 authorization: %w", err)
	}

	return token, nil
}

func attemptTokenRefresh(ctx context.Context, config *oauth2.Config) *oauth2.Token {
	var (
		ok     bool
		expiry string
		err    error
	)

	token := &oauth2.Token{}

	if token.AccessToken, ok = os.LookupEnv("GITLAB_OAUTH_TOKEN"); !ok {
		return nil
	}

	if token.RefreshToken, ok = os.LookupEnv("GITLAB_OAUTH_REFRESH_TOKEN"); !ok {
		return nil
	}

	if expiry, ok = os.LookupEnv("GITLAB_OAUTH_TOKEN_EXPIRATION"); !ok {
		return nil
	}

	if token.TokenType, ok = os.LookupEnv("GITLAB_OAUTH_TOKEN_TYPE"); !ok {
		return nil
	}

	if token.Expiry, err = time.Parse(time.RFC3339, expiry); err != nil {
		return nil
	}

	ts := config.TokenSource(ctx, token)
	client := oauth2.NewClient(ctx, ts)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "https://gitlab.com/api/v4/user", nil)
	if err != nil {
		log.Warn().
			Err(err).
			Msg("❌ Refresh failed, reauthenticating...")

		return nil
	}

	resp, err := client.Do(req) // nolint:bodyclose
	if err != nil {
		log.Warn().
			Err(err).
			Msg("❌ Refresh failed, reauthenticating...")

		return nil
	}

	defer utils.DeferWrapClose(resp.Body)

	if resp.StatusCode >= 400 {
		log.Warn().
			Int("status_code", resp.StatusCode).
			Msg("❌ Refresh failed with invalid HTTP status, reauthenticating...")

		return nil
	}

	newToken, err := ts.Token()
	if err != nil {
		log.Warn().
			Err(err).
			Msg("❌ Refresh failed, reauthenticating...")

		newToken = nil
	}

	return newToken
}
