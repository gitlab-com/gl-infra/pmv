package op

import (
	"errors"
	"fmt"
	"strings"

	"bitbucket.org/creachadair/shell"
	"github.com/bitfield/script"
)

var errDeleteWithEmptyTags = errors.New("cannot delete items with empty tag")

// DeleteItemsByTag will delete any tagged items with the given tag from
// the vault, or all vaults if vault is empty.
func DeleteItemsByTag(tags string, vault string) error {
	if tags == "" {
		return errDeleteWithEmptyTags
	}

	listItemsCmd := []string{
		"op", "item", "list", "--tags", tags, "--format=json",
	}

	if vault != "" {
		listItemsCmd = append(listItemsCmd, "--vault", vault)
	}

	findOutput, err := script.Exec(shell.Join(listItemsCmd)).
		String()

	if err != nil {
		return fmt.Errorf("unable to find previously tagged items: %w", err)
	}

	findOutput = strings.Trim(findOutput, " \n\r\t")
	if findOutput == "[]" {
		// No items to delete...
		return nil
	}

	// There are items, delete them...
	_, err = script.Echo(findOutput).Exec("op item delete -").
		Stdout()

	if err != nil {
		return fmt.Errorf("unable to delete previously tagged items: %w", err)
	}

	return nil
}
