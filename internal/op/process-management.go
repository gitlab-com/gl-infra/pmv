package op

import (
	"errors"
	"fmt"
	"math"
	"os"
	"os/exec"
	"os/user"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/shirou/gopsutil/v4/process"
)

var errUint32Overflow = errors.New("uint32 overflow")

func startFullyDetachedOpDaemon() (err error) {
	log.Debug().
		Msg("op daemon is not running, starting the daemon")

	// This is a kinda whacky way of daemonizing the `op daemon --background` process
	// but I've spent far too much time down the rabbit-hole of performing double-fork daemonization in
	// Go to warrant any further time spent on this. If there's a better way, we can switch it out later.
	// Adapted from the direnv docs at https://github.com/direnv/direnv/wiki/Daemonize
	cmd := exec.Command("sh", "-c", "( ( exec 0</dev/null; exec 1>/dev/null; exec 2>&1; exec 3>&-; exec 4>&-; exec op daemon  --background ) & ) &")
	cmd.Stderr = nil
	cmd.Stdout = nil
	cmd.Stdin = nil

	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to daemonize op daemon: %w", err)
	}

	// Give the process a second to grab the socket to avoid race-conditions from `op`'s attempts to spawn the daemon
	time.Sleep(100 * time.Millisecond)

	log.Info().
		Msg("👻 started the op background daemon")

	return nil
}

func getUID() (uint32, error) {
	u, err := user.Current()
	if err != nil {
		return 0, fmt.Errorf("failed to obtain current user: %w", err)
	}

	// 	On POSIX systems, this is a decimal number representing the uid.
	userIDc, err := strconv.Atoi(u.Uid)
	if err != nil {
		return 0, fmt.Errorf("failed to parse uid: %w", err)
	}

	// Doing this to quieten down gosec linter
	if userIDc > math.MaxUint32 || userIDc < 0 {
		return 0, errUint32Overflow
	}

	userID := uint32(userIDc)

	return userID, nil
}

func findOpDaemon(uid uint32, opPath string) (*process.Process, error) {
	procs, err := process.Processes()
	if err != nil {
		return nil, fmt.Errorf("failed to list processes: %w", err)
	}

	for _, p := range procs {
		if !processOwnedByUser(p, uid) {
			continue
		}

		cmdLine, err := p.Cmdline()

		if err != nil {
			continue
		}

		if strings.HasPrefix(cmdLine, opPath+" daemon") || strings.HasPrefix(cmdLine, "op daemon") {
			return p, nil
		}
	}

	return nil, nil
}

func killOpDaemonIfRunning(uid uint32, opPath string) (bool, error) {
	p, err := findOpDaemon(uid, opPath)
	if err != nil {
		return false, fmt.Errorf("failed to find op daemon: %w", err)
	}

	if p == nil {
		// Our work here is done
		return false, nil
	}

	err = p.Kill()
	if err != nil {
		return false, fmt.Errorf("failed to kill op daemon: %w", err)
	}

	return true, nil
}

func isOpDaemonRunning(uid uint32, opPath string) (bool, error) {
	proc, err := findOpDaemon(uid, opPath)
	if err != nil {
		return false, fmt.Errorf("failed to find op daemon: %w", err)
	}

	if proc == nil {
		return false, nil
	}

	return true, nil
}

func processOwnedByUser(p *process.Process, uid uint32) bool {
	uids, err := p.Uids()
	if err != nil {
		return false
	}

	for _, puid := range uids {
		if puid == uid {
			return true
		}
	}

	return false
}

// isRunningInDirEnv returns true if running in direnv.
func isRunningInDirEnv() bool {
	pid := os.Getpid()
	if pid < 0 || pid > math.MaxInt32 {
		// Appease the linters!
		log.Debug().
			Msg("pid integer overflow")

		return false
	}

	p, err := process.NewProcess(int32(pid))
	if err != nil {
		log.Debug().
			Err(err).
			Msg("failed to load process")

		return false
	}

	for {
		name, err := p.Name()
		if err != nil {
			log.Debug().
				Err(err).
				Msg("failed to get pid name")

			return false
		}

		if name == "direnv" {
			return true
		}

		ppid, err := p.Ppid()
		if err != nil {
			log.Debug().
				Err(err).
				Msg("failed to get ppid")

			return false
		}

		if ppid == 0 {
			return false
		}

		p, err = p.Parent()
		if err != nil {
			log.Debug().
				Err(err).
				Msg("failed to load parent process")

			return false
		}
	}
}
