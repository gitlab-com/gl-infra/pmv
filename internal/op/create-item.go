package op

import (
	"encoding/json"
	"fmt"
	"os"

	"bitbucket.org/creachadair/shell"
	"github.com/bitfield/script"
	"github.com/rs/zerolog/log"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"
)

// CreateItem creates a new user login in the system.
func CreateItem(title string, item *model.OPItem, url string, tags string, vault string) (*model.OPItem, error) {
	jsonString, err := json.Marshal(&item)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall secret: %w", err)
	}

	createCmdArgs := []string{"op", "item", "create", "-", "--format=json"}

	if log.Debug().Enabled() {
		createCmdArgs = append(createCmdArgs, "--debug")
	}

	if url != "" {
		createCmdArgs = append(createCmdArgs, "--url", url)
	}

	if vault != "" {
		createCmdArgs = append(createCmdArgs, "--vault", vault)
	}

	if tags != "" {
		createCmdArgs = append(createCmdArgs, "--tags", tags)
	}

	createCmdArgs = append(createCmdArgs, "--title", title)

	command := shell.Join(createCmdArgs)

	log.Debug().
		Str("command", command).
		Msg("Executing")

	output, err := script.Echo(string(jsonString)).Exec(command).WithStderr(os.Stderr).String()
	if err != nil {
		return nil, fmt.Errorf("op create item Login: %w", err)
	}

	item, err = model.ParseItem(output)
	if err != nil {
		return nil, fmt.Errorf("failed to parse response: %w", err)
	}

	return item, nil
}
