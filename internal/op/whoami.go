package op

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"bitbucket.org/creachadair/shell"
	"github.com/bitfield/script"
	"github.com/rs/zerolog/log"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"
)

var ErrOpWhoAmIFailed = errors.New("1password-cli whoami, `op whoami`, failed.\n" +
	"Please run `op signin --account gitlab.1password.com`.\n" +
	"For further information, visit https://developer.1password.com/docs/cli/reference/commands/signin/")

func WhoAmI() (*model.OPWhoAmI, error) {
	_, err := verifyOpCommand()
	if err != nil {
		return nil, err
	}

	whoCmdArgs := []string{"op", "whoami", "--account", "gitlab.1password.com", "--format=json"}

	if log.Debug().Enabled() {
		whoCmdArgs = append(whoCmdArgs, "--debug")
	}

	command := shell.Join(whoCmdArgs)

	log.Debug().
		Str("command", command).
		Msg("Executing")

	output, err := script.Exec(command).WithStderr(os.Stderr).String()
	if err != nil {
		return nil, errors.Join(fmt.Errorf("op not signed in: %w", err), ErrOpWhoAmIFailed)
	}

	whoami := &model.OPWhoAmI{}

	err = json.Unmarshal([]byte(output), &whoami)
	if err != nil {
		return nil, errors.Join(fmt.Errorf("op not signed in: %w", err), ErrOpWhoAmIFailed)
	}

	return whoami, nil
}
