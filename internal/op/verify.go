package op

import (
	"fmt"
	"os/exec"

	pmverrors "gitlab.com/gitlab-com/gl-infra/pmv/internal/errors"
)

var ErrOpNotOnPath = fmt.Errorf("1password-cli command, `op`, not found on PATH: %w", pmverrors.ErrSetup)

func verifyOpCommand() (string, error) {
	p, err := exec.LookPath("op")
	if err != nil {
		return "", ErrOpNotOnPath
	}

	return p, nil
}
