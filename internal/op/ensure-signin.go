package op

import (
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/rs/zerolog/log"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"
)

func EnsureSignin() (*model.OPWhoAmI, map[string]string, error) {
	opPath, err := verifyOpCommand()
	if err != nil {
		return nil, nil, err
	}

	uid, err := getUID()
	if err != nil {
		return nil, nil, fmt.Errorf("failed to obtain UID/GID, cannot manage op daemon: %w", err)
	}

	// This needs to happen *before* we call op
	precautionsRequired := areDirEnvDaemonizationPrecautionsRequired(uid, opPath)

	var env map[string]string

	whoami, err := WhoAmI()
	if err != nil {
		env, err = opSignin()

		if err != nil {
			return nil, nil, fmt.Errorf("failed to sign into op: %w", err)
		}

		whoami, err = WhoAmI()
		if err != nil {
			return nil, nil, fmt.Errorf("unable to determine op whoami after signin - something is wrong: %w", err)
		}
	}

	if !precautionsRequired {
		return whoami, env, nil
	}

	found, err := killOpDaemonIfRunning(uid, opPath)
	if err != nil {
		log.Warn().
			Err(err).
			Msg("failed to kill op daemon")
	} else if !found {
		log.Debug().
			Err(err).
			Msg("no daemon found to kill")
	}

	err = startFullyDetachedOpDaemon()
	if err != nil {
		log.Warn().
			Err(err).
			Msg("failed to spawn detached op daemon")
	}

	return whoami, env, nil
}

// areDirEnvDaemonizationPrecautionsRequired will return true iff op daemon is not already running AND
// we're running in a direnv environment.
func areDirEnvDaemonizationPrecautionsRequired(uid uint32, opPath string) bool {
	opDaemonRunningAtStart, err := isOpDaemonRunning(uid, opPath)
	if err != nil {
		log.Warn().
			Err(err).
			Msg("unable to determine if opDaemon is running")

		return false
	}

	if opDaemonRunningAtStart {
		log.Debug().
			Msg("op daemon is already running, no need to spawn another instance")

		return false
	}

	result := isRunningInDirEnv()
	if result {
		log.Debug().
			Msg("detected that pmv is running in direnv: special daemon handling measures will be in place")
	} else {
		log.Debug().
			Msg("op daemon is not running, but pmv is not running within direnv: skipping daemonization")
	}

	return result
}

// opSignin executes `op signin` returning any environment variables returned
// by the signin process.
func opSignin() (map[string]string, error) {
	signinArgs := []string{"signin", "--account", "gitlab.1password.com"}

	if log.Debug().Enabled() {
		signinArgs = append(signinArgs, "--debug")
	}

	log.Debug().
		Str("cmd", "op").
		Strs("args", signinArgs).
		Msg("performing signin")

	cmd := exec.Command("op", signinArgs...)
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr

	// Execute the command and get the output
	output, err := cmd.Output()
	if err != nil {
		return nil, fmt.Errorf("failed to sign into op: %w", err)
	}

	// Parse the output from `op signin`
	lines := strings.Split(string(output), "\n")
	env := map[string]string{}

	for _, s := range lines {
		s = strings.TrimSpace(s)

		if strings.HasPrefix(s, "#") {
			continue
		}

		envKey, envValue, found := strings.Cut(s, "=")
		if !found {
			continue
		}

		// Export into the local environment
		os.Setenv(envKey, envValue)

		env[envKey] = envValue
	}

	return env, nil
}
