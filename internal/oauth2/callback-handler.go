package pmvoauth2

import (
	"errors"
	"io"
	"net/http"

	"github.com/rs/zerolog/log"

	"golang.org/x/oauth2"
)

var errExchangeFailed = errors.New("oauth exchange failed")

type oauthRedirectHandler struct {
	codeVerifier string
	oauthConfig  *oauth2.Config
	errChan      chan error
	token        *oauth2.Token
}

func textResponse(rw http.ResponseWriter, status int, body string) {
	rw.Header().Add("Content-Type", "text/plain")
	rw.WriteHeader(status)

	_, err := io.WriteString(rw, body)
	if err != nil {
		log.Printf("failed to write body: %v", err)
	}
}

func (h *oauthRedirectHandler) ServeHTTP(rw http.ResponseWriter, request *http.Request) {
	query := request.URL.Query()

	code := query.Get("code")
	if code == "" {
		textResponse(rw, http.StatusBadRequest, "Missing Code")
		h.errChan <- errExchangeFailed

		return
	}

	token, err := h.oauthConfig.Exchange(
		request.Context(),
		code,
		oauth2.VerifierOption(h.codeVerifier),
	)

	if err != nil {
		textResponse(rw, http.StatusInternalServerError, err.Error())
		h.errChan <- err

		return
	}

	textResponse(rw, http.StatusOK, "Authentication complete. You can close this window.")

	h.token = token
	h.errChan <- nil
}
