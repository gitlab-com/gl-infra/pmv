package pmvoauth2

import (
	"context"
	"errors"

	"github.com/rs/zerolog/log"

	"net/http"
	"time"

	"github.com/skratchdot/open-golang/open"
	"golang.org/x/oauth2"
)

func PerformOAuthExchange(ctx context.Context, config *oauth2.Config) (*oauth2.Token, error) {
	config.RedirectURL = "http://localhost:33821/callback"

	// use PKCE to protect against CSRF attacks
	// https://www.ietf.org/archive/id/draft-ietf-oauth-security-topics-22.html#name-countermeasures-6
	verifier := oauth2.GenerateVerifier()

	// Redirect user to consent page to ask for permission
	// for the scopes specified above.
	url := config.AuthCodeURL("state", oauth2.AccessTypeOffline, oauth2.S256ChallengeOption(verifier))

	handler := &oauthRedirectHandler{
		codeVerifier: verifier,
		oauthConfig:  config,
		errChan:      make(chan error),
	}

	err := open.Run(url)
	if err != nil {
		log.Warn().
			Err(err).
			Str("link", url).
			Msg("Failed to open link")
	}

	mux := http.NewServeMux()
	mux.Handle("/callback", handler)

	srv := &http.Server{
		Addr:              "localhost:33821",
		ReadHeaderTimeout: 5 * time.Second,
	}
	srv.Handler = mux

	go func() {
		err = srv.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatal().Err(err).Msg("http: ListenAndServe failed")
		}
	}()

	err = <-handler.errChan
	if err != nil {
		return nil, err
	}

	err = srv.Shutdown(ctx)
	if err != nil {
		log.Warn().Err(err).Msg("failed to shutdown server: %v")
	}

	return handler.token, nil
}
