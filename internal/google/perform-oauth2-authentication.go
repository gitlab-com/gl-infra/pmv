package google

import (
	"context"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/utils"

	"golang.org/x/oauth2"

	googleoauth "golang.org/x/oauth2/google"

	pmvoauth "gitlab.com/gitlab-com/gl-infra/pmv/internal/oauth2"
)

var errInvalidOrganization = errors.New("invalid organization")

// Be aware: the "client secret" is a misnomer... it _is_ allowed to be public. In fact, for a desktop app it has to be
// The Google Developer docs state:
// > The process results in a client ID and, in some cases, a client secret, which you embed in the source code
// > of your application. (In this context, the client secret is obviously not treated as a secret.)
// https://bit.ly/43P4ayw
// Additionally, this oauth2 app is internal-only, meaning it can only be used by GitLab Team Members.
// App is configured at https://bit.ly/44Kngr9, in GCP Project `gitlab-infra-automation`.

type appDetails struct {
	clientID     string
	clientSecret string
}

var organizationApps = map[string]appDetails{
	"gitlab.com": {
		clientID:     "154365923060-ca3te1dejqt3pf7ksc84eat9mh9mf2f7.apps.googleusercontent.com", // gitleaks:allow
		clientSecret: "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",                                      // nolint:gosec // gitleaks:allow
	},
	// Dedicated GCP Dev Organization
	"gitlab-private.org": {
		clientID:     "346872993037-a0vn662bqtdr4guh1ld90n19j0ig1qs9.apps.googleusercontent.com", // gitleaks:allow
		clientSecret: "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",                                      // nolint:gosec // gitleaks:allow
	},
}

// PerformOAuth2Authentication uses Authorization code with Proof Key for Code Exchange (PKCE) to perform
// a secret-less OAuth2 authentication.
func PerformOAuth2Authentication(ctx context.Context, organization string) (*oauth2.Token, error) {
	gcpClientID, gcpClientSecret, err := lookupCredentials(organization)
	if err != nil {
		return nil, err
	}

	config := &oauth2.Config{
		ClientID:     gcpClientID,
		ClientSecret: gcpClientSecret,
		Endpoint:     googleoauth.Endpoint,
		Scopes: []string{
			// See https://developers.google.com/identity/protocols/oauth2/scopes
			// for a list of scopes
			// TODO: make scopes configurable...
			"https://www.googleapis.com/auth/cloud-platform",
			"https://www.googleapis.com/auth/compute",
			"https://www.googleapis.com/auth/iam",

			"openid",
			"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/userinfo.email",

			"https://www.googleapis.com/auth/accounts.reauth",
		},
	}

	existing := attemptTokenRefresh(ctx, config)
	if existing != nil {
		log.Info().
			Msg("♻️  Reusing existing token...")

		return existing, nil
	}

	token, err := pmvoauth.PerformOAuthExchange(ctx, config)
	if err != nil {
		return nil, fmt.Errorf("unable to perform oauth2 authorization: %w", err)
	}

	return token, nil
}

func lookupCredentials(organization string) (string, string, error) {
	d, ok := organizationApps[organization]
	if !ok {
		return "", "", fmt.Errorf("unknown organization '%s': %w", organization, errInvalidOrganization)
	}

	return d.clientID, d.clientSecret, nil
}

func attemptTokenRefresh(ctx context.Context, config *oauth2.Config) *oauth2.Token {
	var (
		ok     bool
		expiry string
		err    error
	)

	token := &oauth2.Token{}

	if token.AccessToken, ok = os.LookupEnv("GOOGLE_OAUTH_ACCESS_TOKEN"); !ok {
		return nil
	}

	if token.RefreshToken, ok = os.LookupEnv("GOOGLE_OAUTH_REFRESH_TOKEN"); !ok {
		return nil
	}

	if expiry, ok = os.LookupEnv("GOOGLE_TOKEN_EXPIRATION"); !ok {
		return nil
	}

	if token.TokenType, ok = os.LookupEnv("GOOGLE_OAUTH_TOKEN_TYPE"); !ok {
		return nil
	}

	if token.Expiry, err = time.Parse(time.RFC3339, expiry); err != nil {
		return nil
	}

	ts := config.TokenSource(ctx, token)
	client := oauth2.NewClient(ctx, ts)

	resp, err := client.Get("https://www.googleapis.com/oauth2/v3/tokeninfo")
	if err != nil {
		log.Warn().
			Err(err).
			Msg("❌ Refresh failed, reauthenticating...")

		return nil
	}

	if resp.Body != nil {
		defer utils.DeferWrapClose(resp.Body)
	}

	if resp.StatusCode >= 400 {
		log.Warn().
			Int("status_code", resp.StatusCode).
			Msg("❌ Refresh failed for invalid HTTP code, reauthenticating...")

		return nil
	}

	newToken, err := ts.Token()
	if err != nil {
		// Failed to retrieve token, reauthenticate
		log.Warn().
			Err(err).
			Msg("❌ Refresh failed, reauthenticating...")

		newToken = nil
	}

	return newToken
}
