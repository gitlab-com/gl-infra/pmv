package aws

import (
	"fmt"

	"github.com/rs/zerolog/log"

	"github.com/aws/aws-sdk-go/service/sts"
)

// GetSessionToken performs an MFA authentication for the AWS API.
// It returns the new credentials to be used with the MFA token.
func GetSessionToken(accessKey string, secretAccessKey string, serialNumber string, tokenCode string, region string) (*sts.Credentials, error) {
	sess, err := createSession(accessKey, secretAccessKey, "", region)
	if err != nil {
		return nil, fmt.Errorf("failed to create session: %w", err)
	}

	svc := sts.New(sess)

	log.Debug().
		Str("serial_number", serialNumber).
		Str("token_code", tokenCode).
		Msg("Fetching STS Token")

	input := &sts.GetSessionTokenInput{
		SerialNumber: &serialNumber,
		TokenCode:    &tokenCode,
	}

	result, err := svc.GetSessionToken(input)
	if err != nil {
		return nil, fmt.Errorf("failed to obtain session token: %w", err)
	}

	return result.Credentials, nil
}
