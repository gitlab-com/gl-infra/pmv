package aws

import (
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/sts"
)

// VerifyCallerIdentity will verify the caller identity given creds,
// and return some useful information about the account.
func VerifyCallerIdentity(accessKey string, secretAccessKey string, sessionToken string) (username string,
	awsAccountID string, accountAlias string, err error) {
	sess, err := createSession(accessKey, secretAccessKey, sessionToken, "")
	if err != nil {
		return "", "", "", fmt.Errorf("failed to create session: %w", err)
	}

	svc := sts.New(sess)
	input := &sts.GetCallerIdentityInput{}

	result, err := svc.GetCallerIdentity(input)
	if err != nil {
		return "", "", "", fmt.Errorf("failed to verify caller identity: %w", err)
	}

	awsAccountID = *result.Account

	arnSplit := strings.Split(*result.Arn, "/")
	if len(arnSplit) == 2 {
		username = arnSplit[1]
	}

	iamService := iam.New(sess)
	aliasOutput, err := iamService.ListAccountAliases(&iam.ListAccountAliasesInput{})

	// If this errors, ignore the error...
	if err != nil {
		// There should only ever be one alias....
		if len(aliasOutput.AccountAliases) > 0 {
			accountAlias = *aliasOutput.AccountAliases[0]
		}
	}

	return username, awsAccountID, accountAlias, nil
}
