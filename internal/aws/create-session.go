package aws

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

func createSession(accessKey string, secretAccessKey string, sessionToken string, region string) (*session.Session, error) {
	// Ensure that we don't accidentally use other credentials from the environment
	os.Unsetenv("AWS_ACCESS_KEY_ID")
	os.Unsetenv("AWS_SECRET_ACCESS_KEY")
	os.Unsetenv("AWS_SESSION_TOKEN")

	config := aws.NewConfig().
		WithCredentialsChainVerboseErrors(true).
		WithCredentials(credentials.NewStaticCredentials(accessKey, secretAccessKey, sessionToken))

	var sess *session.Session

	var err error

	if region != "" {
		config = config.WithRegion(region)
	}

	sess, err = session.NewSession(config)

	if err != nil {
		return nil, fmt.Errorf("failed to create session: %w", err)
	}

	return sess, nil
}

func createPreconfiguredSession() (*session.Session, error) {
	config := aws.NewConfig().WithCredentialsChainVerboseErrors(true)

	sess, err := session.NewSession(config)
	if err != nil {
		return nil, fmt.Errorf("failed to create session: %w", err)
	}

	return sess, nil
}
