package aws

import (
	"bufio"
	"fmt"
	"os"
	"time"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/secureentry"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/charmbracelet/huh/spinner"
	"github.com/pquerna/otp"
	opttotp "github.com/pquerna/otp/totp"
	log "github.com/rs/zerolog/log"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/totp"
)

// Randomly generated UUID For provider field.
const providerIdentifierFieldID = "odaeufe3gvvgxicoylgyl5g5na"
const mfaIdentifierFieldID = "2xchxnwotzc3oljecpkb5l6d4e"

// CreateMFA creates a Virtual MFA and returns one-password fields for the MFA ID.
func CreateMFA(accessKey string, secretAccessKey string, username string, provider totp.TOTPProvider, reader *bufio.Reader) ([]*model.OPField, error) {
	sess, err := createSession(accessKey, secretAccessKey, "", "")
	if err != nil {
		return nil, fmt.Errorf("failed to create session: %w", err)
	}

	iamService := iam.New(sess)

	assignedMFAs, err := listVirtualMFADevices(iamService, username)
	if err != nil {
		return nil, fmt.Errorf("failed to discover assigned devices: %w", err)
	}

	assignedMFAIDs := make([]string, len(assignedMFAs))
	for i, v := range assignedMFAs {
		assignedMFAIDs[i] = aws.StringValue(v.SerialNumber)
	}

	foundMFAID := ""

	if len(assignedMFAIDs) >= 0 {
		foundMFAID, err = provider.Lookup(assignedMFAIDs)
		if err != nil {
			return nil, fmt.Errorf("totp lookup failed: %w", err)
		}
	}

	// If we found an MFA, return that MFA to the caller
	if foundMFAID != "" {
		err = confirmNewToken(accessKey, secretAccessKey, provider, foundMFAID)
		if err != nil {
			return nil, fmt.Errorf("failed to confirm TOTP token: %w", err)
		}

		return fieldsForMFA(provider, foundMFAID), nil
	}

	// From here on out, we need to create a new MFA.
	log.Info().Msg("🙅🏻‍♂️ No MFA token found: let's set you up with a new one...")
	log.Info().Msg("📗 Review the instructions at https://gitlab.com/gitlab-com/gl-infra/pmv/-/blob/main/docs/aws-mfa.md")
	log.Info().Msg("🌐 - or if you know what you're doing, visit " +
		"https://us-east-1.console.aws.amazon.com/iam/home#/security_credentials?section=IAM_credentials")

	mfaSecretKey, err := promptForSecretKeyAndGenerateCodes()
	if err != nil {
		return nil, fmt.Errorf("failed to wait for new unassigned MFA: %w", err)
	}

	deviceSerialNumber := waitForNewDeviceWithSpinner(iamService, username, assignedMFAs)

	log.Info().
		Str("serial_number", deviceSerialNumber).
		Msg("📲 Discovered new device")

	// Store the secret key on the device
	err = provider.Store(deviceSerialNumber, mfaSecretKey)
	if err != nil {
		return nil, fmt.Errorf("totp Store failed: %w", err)
	}

	err = confirmNewToken(accessKey, secretAccessKey, provider, deviceSerialNumber)
	if err != nil {
		return nil, err
	}

	return fieldsForMFA(provider, deviceSerialNumber), nil
}

func fieldsForMFA(provider totp.TOTPProvider, deviceSerialNumber string) []*model.OPField {
	return []*model.OPField{
		{
			ID:    providerIdentifierFieldID,
			Label: "TOTP Provider",
			Value: provider.ID(),
		},
		{
			ID:    mfaIdentifierFieldID,
			Label: "AWS Virtual MFA Arn",
			Value: deviceSerialNumber,
		},
	}
}

func waitForNewDeviceWithSpinner(iamService *iam.IAM, username string, assignedMFAs []*iam.VirtualMFADevice) string {
	var device *iam.VirtualMFADevice

	_ = spinner.New().Output(os.Stderr).Title("Waiting for new Virtual MFA device to be assigned...").Action(func() {
		var err error

		device, err = waitForNewAssignedVirtualMFA(iamService, username, assignedMFAs)
		if err != nil {
			log.Error().
				Err(err).
				Msg("❌ failed to wait for new unassigned MFA")
		}
	}).Run()

	return aws.StringValue(device.SerialNumber)
}

func confirmNewToken(accessKey string, secretAccessKey string, provider totp.TOTPProvider, deviceSerialNumber string) error {
	log.Info().
		Msg("✅ Ok. Let's confirm that your TOTP token is working.")

	retrievedCode, err := provider.Retrieve(deviceSerialNumber)
	if err != nil {
		return fmt.Errorf("totp retrieve failed: %w", err)
	}

	_, err = GetSessionToken(accessKey, secretAccessKey, deviceSerialNumber, retrievedCode, "")
	if err != nil {
		return fmt.Errorf("totp exchange failed: %w", err)
	}

	return nil
}

func promptForSecretKeyAndGenerateCodes() (string, error) {
	fmt.Print("pmv: 🔓 Enter MFA Secret Code: ")

	mfaSecretKey, err := secureentry.SecureRead()
	if err != nil {
		return "", fmt.Errorf("failed to read secret: %w", err)
	}

	code1, code2, err := waitForTwoTokenCodes(mfaSecretKey)
	if err != nil {
		return "", err
	}

	log.Info().
		Str("code_1", code1).
		Str("code_2", code2).
		Msg("📲 Return to your browser, and enter these confirmation codes in the AWS Console New Virtual MFA confirmation dialog")

	return mfaSecretKey, nil
}

// Waits for two TOTP tokens to be generated.
func waitForTwoTokenCodes(mfaSecretKey string) (string, string, error) {
	code1, err := getCode(mfaSecretKey)
	if err != nil {
		return "", "", fmt.Errorf("failed to generate code: %w", err)
	}

	log.Info().
		Str("code_1", code1).
		Msg("📲 Confirmation Code #1")

	var code2 string

	_ = spinner.New().Output(os.Stderr).Title("Waiting up to 30 seconds for next code...").Action(func() {
		for {
			code2, err = getCode(mfaSecretKey)
			if err != nil {
				log.Error().
					Err(err).
					Msg("Code generation failed")
			}

			if code2 != code1 {
				return
			}

			time.Sleep(1 * time.Second)
		}
	}).Run()

	log.Info().
		Str("code_2", code2).
		Msg("📲 Confirmation Code #2")

	return code1, code2, nil
}

// Waits for a new unassigned VirtualMFA to appear in the list.
func waitForNewAssignedVirtualMFA(iamService *iam.IAM, username string, existingDevices []*iam.VirtualMFADevice) (*iam.VirtualMFADevice, error) {
	existingSet := map[string]struct{}{}

	for _, v := range existingDevices {
		existingSet[aws.StringValue(v.SerialNumber)] = struct{}{}
	}

	for {
		newUnassigned, err := checkForNewAssignedVirtualMFA(iamService, username, existingSet)
		if err != nil {
			return nil, fmt.Errorf("failed to check for new unassigned devices: %w", err)
		}

		if newUnassigned != nil {
			return newUnassigned, nil
		}

		time.Sleep(10 * time.Second)
	}
}

// Given a set of known existing MFA devices, polls AWS until a new device is registered, and then returns that device.
func checkForNewAssignedVirtualMFA(iamService *iam.IAM, username string, existingSet map[string]struct{}) (*iam.VirtualMFADevice, error) {
	input := &iam.ListVirtualMFADevicesInput{AssignmentStatus: aws.String(iam.AssignmentStatusTypeAssigned)}

	var found *iam.VirtualMFADevice

	err := iamService.ListVirtualMFADevicesPages(input, func(listResponse *iam.ListVirtualMFADevicesOutput, lastPage bool) bool {
		for _, v := range listResponse.VirtualMFADevices {
			if v.User == nil || v.EnableDate == nil {
				continue
			}

			serial := aws.StringValue(v.SerialNumber)

			// already in set, ignore
			if _, ok := existingSet[serial]; ok {
				continue
			}

			if aws.StringValue(v.User.UserName) == username {
				found = v
				return false
			}
		}

		return true
	})
	if err != nil {
		return nil, fmt.Errorf("failed to list devices: %w", err)
	}

	return found, nil
}

// List all VirtualMFA devices assigned to the current user.
func listVirtualMFADevices(iamService *iam.IAM, username string) (assigned []*iam.VirtualMFADevice, err error) {
	assigned = nil

	listResponse, err := iamService.ListVirtualMFADevices(&iam.ListVirtualMFADevicesInput{})
	if err != nil {
		return nil, fmt.Errorf("failed to list devices: %w", err)
	}

	for _, v := range listResponse.VirtualMFADevices {
		if v.User == nil {
			continue
		}

		if v.User.UserName == nil || *v.User.UserName != username {
			// This MFA is not for you. Move on.
			continue
		}

		if v.EnableDate != nil {
			assigned = append(assigned, v)
		}
	}

	return assigned, nil
}

// Generates a code for an TOTP Secret Key.
func getCode(secret string) (string, error) {
	passcode, err := opttotp.GenerateCodeCustom(secret, time.Now(), opttotp.ValidateOpts{
		Period:    30,
		Skew:      1,
		Digits:    otp.DigitsSix,
		Algorithm: otp.AlgorithmSHA1,
	})
	if err != nil {
		return "", fmt.Errorf("failed to generate custom code: %w", err)
	}

	return passcode, nil
}
