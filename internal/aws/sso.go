package aws

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go/service/sts"
	"github.com/rs/zerolog/log"

	"os"
	"os/user"
	"path"
	"regexp"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sso"
	"github.com/aws/aws-sdk-go/service/ssooidc"
	"github.com/skratchdot/open-golang/open"
)

// maxCreateTokenAttempts is the number of times to retry the SSO endpoint.
const maxCreateTokenAttempts = 120

const authRegion = "us-east-1"

const pmvCacheDir = ".pmv"

var nonAlphanumericRegex = regexp.MustCompile(`[^a-zA-Z0-9 ]+`)

// PerformSSOAuthentication follows the process described in
// https://docs.aws.amazon.com/cli/latest/reference/sso-oidc/index.html#description
// to perform an OIDC exchange for temporary a temporary token.
func PerformSSOAuthentication(startURL string, accountID string, roleName string, assumeRoleARNs []string) (*sts.Credentials, error) {
	config := aws.NewConfig().
		WithCredentialsChainVerboseErrors(true)

	sess, err := session.NewSession(config)
	if err != nil {
		return nil, fmt.Errorf("failed to create session: %w", err)
	}

	// Create a SSOOIDC client with additional configuration
	svc := ssooidc.New(sess, aws.NewConfig().WithRegion(authRegion))

	clientID, clientSecret, err := getClientDetails(svc, startURL)
	if err != nil {
		return nil, fmt.Errorf("failed to register client: %w", err)
	}

	authOutput, err := svc.StartDeviceAuthorization(&ssooidc.StartDeviceAuthorizationInput{
		ClientId:     aws.String(clientID),
		ClientSecret: aws.String(clientSecret),
		StartUrl:     aws.String(startURL),
	})
	if err != nil {
		return nil, fmt.Errorf("failed to start auth: %w", err)
	}

	completeURL := *authOutput.VerificationUriComplete
	log.Info().Str("link", completeURL).Msg("🧭 visiting link")
	log.Info().Msg("🧭 please complete authorization online")

	err = open.Run(completeURL)
	if err != nil {
		log.Warn().
			Err(err).
			Str("link", completeURL).
			Msg("Failed to open link")
	}

	tokenOutput, err := pollForSession(svc, clientID, clientSecret, authOutput)
	if err != nil {
		return nil, fmt.Errorf("failed to obtain token: %w", err)
	}

	ssoSvc := sso.New(sess, aws.NewConfig().WithRegion(authRegion))

	creds, err := ssoSvc.GetRoleCredentials(&sso.GetRoleCredentialsInput{
		AccessToken: tokenOutput.AccessToken,
		AccountId:   aws.String(accountID),
		RoleName:    aws.String(roleName),
	})
	if err != nil {
		return nil, fmt.Errorf("failed to get role credentials: %w", err)
	}

	stsCreds, err := traverseRoleAssumptionsForSSO(creds, assumeRoleARNs)
	if err != nil {
		return nil, fmt.Errorf("failed to assume roles: %w", err)
	}

	return stsCreds, nil
}

func traverseRoleAssumptionsForSSO(creds *sso.GetRoleCredentialsOutput, assumeRoleARNs []string) (*sts.Credentials, error) {
	expiration := time.UnixMilli(*creds.RoleCredentials.Expiration)
	stsCreds := &sts.Credentials{
		AccessKeyId:     creds.RoleCredentials.AccessKeyId,
		SecretAccessKey: creds.RoleCredentials.SecretAccessKey,
		SessionToken:    creds.RoleCredentials.SessionToken,
		Expiration:      &expiration,
	}

	var err error

	for _, arn := range assumeRoleARNs {
		accessKeyID := aws.StringValue(stsCreds.AccessKeyId)
		secretAccessKey := aws.StringValue(stsCreds.SecretAccessKey)
		sessionToken := aws.StringValue(stsCreds.SessionToken)

		stsCreds, err = AssumeRole(accessKeyID, secretAccessKey, sessionToken, arn, "us-east-1")
		if err != nil {
			return nil, fmt.Errorf("failed to assumerole for %s: %w", arn, err)
		}
	}

	return stsCreds, nil
}

func getClientName() string {
	parts := []string{"pmv"}

	hostname, err := os.Hostname()
	if err == nil {
		parts = append(parts, stripNonAlphaChars(hostname))
	}

	username, err := user.Current()
	if err == nil {
		parts = append(parts, stripNonAlphaChars(username.Username))
	}

	return strings.Join(parts, "-")
}

// Returns the saved clientID and clientSecret, or if it cannot find them
// creates a new public client.
func getClientDetails(svc *ssooidc.SSOOIDC, startURL string) (string, string, error) {
	clientOutput, err := readCachedClientDetails(startURL)
	if err == nil {
		expired := time.Unix(*clientOutput.ClientSecretExpiresAt, 0)
		if time.Now().Before(expired) {
			return *clientOutput.ClientId, *clientOutput.ClientSecret, nil
		}

		log.Info().Msg("🚫 cached client credentials have expired, recreating")
	}

	clientOutput, err = svc.RegisterClient(&ssooidc.RegisterClientInput{
		ClientName: aws.String(getClientName()),
		ClientType: aws.String("public"),
		Scopes:     aws.StringSlice([]string{"sso:account:access"}),
	})
	if err != nil {
		return "", "", fmt.Errorf("failed to register client: %w", err)
	}

	err = writeCachedClientDetails(clientOutput, startURL)
	if err != nil {
		log.Warn().Err(err).Msg("failed to write sso client cache")
	}

	return *clientOutput.ClientId, *clientOutput.ClientSecret, nil
}

func getCachedClientPath(startURL string) (string, error) {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return "", fmt.Errorf("failed to read home directory: %w", err)
	}

	oidcCacheFilePath := path.Join(homeDir, pmvCacheDir, "sso-client-"+stripNonAlphaChars(startURL))

	return oidcCacheFilePath, nil
}

func ensurePMVCacheDir() error {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return fmt.Errorf("failed to read home directory: %w", err)
	}

	pmvCacheDir := path.Join(homeDir, pmvCacheDir)

	err = os.MkdirAll(pmvCacheDir, 0700)
	if err != nil {
		return fmt.Errorf("failed to read home directory: %w", err)
	}

	return nil
}

func readCachedClientDetails(startURL string) (*ssooidc.RegisterClientOutput, error) {
	oidcCacheFilePath, err := getCachedClientPath(startURL)
	if err != nil {
		return nil, err
	}

	data, err := os.ReadFile(oidcCacheFilePath)
	if err != nil {
		return nil, fmt.Errorf("failed to read cache: %w", err)
	}

	var cached ssooidc.RegisterClientOutput

	err = json.Unmarshal(data, &cached)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal cache: %w", err)
	}

	return &cached, nil
}

func writeCachedClientDetails(clientOutput *ssooidc.RegisterClientOutput, startURL string) error {
	err := ensurePMVCacheDir()
	if err != nil {
		return err
	}

	oidcCacheFilePath, err := getCachedClientPath(startURL)
	if err != nil {
		return err
	}

	data, err := json.Marshal(*clientOutput)
	if err != nil {
		return fmt.Errorf("failed to marshal json: %w", err)
	}

	err = os.WriteFile(oidcCacheFilePath, data, 0600)
	if err != nil {
		return fmt.Errorf("failed to write file: %w", err)
	}

	return nil
}

func pollForSession(svc *ssooidc.SSOOIDC,
	clientID string, clientSecret string, authOutput *ssooidc.StartDeviceAuthorizationOutput) (*ssooidc.CreateTokenOutput, error) {
	count := 0

	for {
		tokenInput := &ssooidc.CreateTokenInput{
			ClientId:     aws.String(clientID),
			ClientSecret: aws.String(clientSecret),
			Code:         authOutput.UserCode,
			DeviceCode:   authOutput.DeviceCode,
			GrantType:    aws.String("urn:ietf:params:oauth:grant-type:device_code"),
		}

		tokenOutput, err := svc.CreateToken(tokenInput)

		if err != nil {
			var aerr awserr.Error
			if errors.As(err, &aerr) {
				if aerr.Code() == ssooidc.ErrCodeAuthorizationPendingException && count < maxCreateTokenAttempts {
					count++

					time.Sleep(time.Duration(*authOutput.Interval) * time.Second)

					continue
				}
			}

			return nil, fmt.Errorf("failed to obtain token: %w", err)
		}

		return tokenOutput, nil
	}
}

func stripNonAlphaChars(str string) string {
	return nonAlphanumericRegex.ReplaceAllString(str, "")
}
