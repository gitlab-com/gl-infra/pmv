package aws

import (
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/sts"
)

// AssumeRole will perform an sts:AssumeRole.
func AssumeRole(accessKey string, secretAccessKey string, sessionToken string, roleArn string, region string) (*sts.Credentials, error) {
	sess, err := createSession(accessKey, secretAccessKey, sessionToken, region)
	if err != nil {
		return nil, fmt.Errorf("failed to create session: %w", err)
	}

	// Allow roleName to be used
	if !strings.HasPrefix(roleArn, "arn:") {
		var roleOutput *iam.GetRoleOutput

		iamSvc := iam.New(sess)

		roleOutput, err = iamSvc.GetRole(&iam.GetRoleInput{
			RoleName: aws.String(roleArn),
		})
		if err != nil {
			return nil, fmt.Errorf("failed to resolve role arn: %w", err)
		}

		roleArn = *roleOutput.Role.Arn
	}

	input := &sts.AssumeRoleInput{
		DurationSeconds: aws.Int64(3600),
		RoleArn:         aws.String(roleArn),
		RoleSessionName: aws.String("pmv-env-with-assume-role"),
	}

	svc := sts.New(sess)

	result, err := svc.AssumeRole(input)
	if err != nil {
		return nil, fmt.Errorf("failed to assume role: %w", err)
	}

	return result.Credentials, nil
}
