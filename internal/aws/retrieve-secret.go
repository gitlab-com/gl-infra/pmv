package aws

import (
	"fmt"

	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

// RetrieveSecret creates a Virtual MFA.
func RetrieveSecret(secretID string) (string, error) {
	sess, err := createPreconfiguredSession()
	if err != nil {
		return "", fmt.Errorf("failed to create session: %w", err)
	}

	secretsmanagerService := secretsmanager.New(sess)

	output, err := secretsmanagerService.GetSecretValue(&secretsmanager.GetSecretValueInput{
		SecretId: &secretID,
	})
	if err != nil {
		return "", fmt.Errorf("failed to fetch secret: %w", err)
	}

	return *output.SecretString, nil
}
