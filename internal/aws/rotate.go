package aws

import (
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/iam"
)

var errRotationFailed = errors.New("credential rotation failed")

// CreateNewAccessKey creates a new credential.
func CreateNewAccessKey(accessKey string, secretAccessKey string, sessionToken string) (string, string, error) {
	sess, err := createSession(accessKey, secretAccessKey, sessionToken, "")
	if err != nil {
		return "", "", fmt.Errorf("failed to create session: %w", err)
	}

	iamService := iam.New(sess)

	output, err := iamService.CreateAccessKey(&iam.CreateAccessKeyInput{})
	if err != nil {
		return "", "", fmt.Errorf("failed to create new access key: %w: %w", err, errRotationFailed)
	}

	return *output.AccessKey.AccessKeyId, *output.AccessKey.SecretAccessKey, nil
}

// DisableAccessKey disables an old access key.
func DisableAccessKey(accessKey string, secretAccessKey string, sessionToken string) error {
	sess, err := createSession(accessKey, secretAccessKey, sessionToken, "")
	if err != nil {
		return fmt.Errorf("failed to create session: %w", err)
	}

	iamService := iam.New(sess)

	_, err = iamService.UpdateAccessKey(&iam.UpdateAccessKeyInput{
		AccessKeyId: aws.String(accessKey),
		Status:      aws.String(string(iam.StatusTypeInactive)),
	})
	if err != nil {
		return fmt.Errorf("failed to inactivate old access key: %w: %w", err, errRotationFailed)
	}

	return nil
}

// DeleteAccessKey deleted an access key.
func DeleteAccessKey(accessKey string, secretAccessKey string, sessionToken string) error {
	sess, err := createSession(accessKey, secretAccessKey, sessionToken, "")
	if err != nil {
		return fmt.Errorf("failed to create session: %w", err)
	}

	iamService := iam.New(sess)

	_, err = iamService.DeleteAccessKey(&iam.DeleteAccessKeyInput{
		AccessKeyId: aws.String(accessKey),
	})
	if err != nil {
		return fmt.Errorf("failed to delete old access key: %w: %w", err, errRotationFailed)
	}

	return nil
}
