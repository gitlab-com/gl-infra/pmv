package export

import (
	"fmt"
	"io"
	"os"

	log "github.com/rs/zerolog/log"

	shellescape "al.essio.dev/pkg/shellescape"
)

// bash will export an environment map in a bash/zsh/sh supported manner.
func bash(envMap map[string]string, unsetMap []string, writer io.Writer, envPrefix string) error {
	if writer == nil {
		return nil
	}

	keys := sortKeys(envMap)

	// Iterate over sorted keys
	for _, k := range keys {
		v := envMap[k]

		_, err := fmt.Fprintf(writer, "export %v%v=%v\n", envPrefix, k, shellescape.Quote(v))
		if err != nil {
			return fmt.Errorf("write failed: %w", err)
		}
	}

	// If the user has a left-over AWS_SESSION_TOKEN from
	// another a previous invocation, clear it to avoid confusion,
	// but also warn of doing so
	for _, v := range unsetMap {
		envVarToClear := envPrefix + v
		_, setInMap := envMap[v]
		currentSessionToken, existsInEnv := os.LookupEnv(envVarToClear)

		if !setInMap && existsInEnv && currentSessionToken != "" {
			log.Warn().
				Str("var", envVarToClear).
				Msg("⚠️  Note: clearing dangling value")

			_, err := fmt.Fprintf(writer, "export %s=\n", envVarToClear)
			if err != nil {
				return fmt.Errorf("write failed: %w", err)
			}
		}
	}

	return nil
}
