package export

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var shellFixtures = []struct {
	shell     string
	envMap    map[string]string
	envPrefix string
	wantOut   string
}{
	{"/usr/local/bin/bash", map[string]string{"key": "value"}, "", "export key=value\n"},
	{"bash", map[string]string{"key": "value"}, "", "export key=value\n"},
	{"/bin/sh", map[string]string{"key": "value"}, "", "export key=value\n"},
	{"/opt/zsh", map[string]string{"key": "value"}, "", "export key=value\n"},
	{"zsh", map[string]string{"key": "value"}, "", "export key=value\n"},
	{"/usr/bin/fish", map[string]string{"key": "value"}, "", "set --export key value;\n"},
	{"fish", map[string]string{"key": "value"}, "", "set --export key value;\n"},
	{"/opt/unknown", map[string]string{"key": "value"}, "", "export key=value\n"},
}

// nolint:paralleltest
func TestShell_SpecifiedShell(t *testing.T) {
	for _, tt := range shellFixtures {
		t.Run(tt.shell, func(t *testing.T) {
			writer := &bytes.Buffer{}

			err := Shell(tt.envMap, nil, writer, tt.envPrefix, tt.shell)
			require.NoError(t, err, "Shell() error = %v", err)

			gotOut := writer.String()
			assert.Equal(t, tt.wantOut, gotOut, "Shell() output = %v, wantOut %v", gotOut, tt.wantOut)
		})
	}
}

// nolint:paralleltest
func TestShell_NoSpecifiedShell(t *testing.T) {
	for _, tt := range shellFixtures {
		t.Run(tt.shell, func(t *testing.T) {
			writer := &bytes.Buffer{}

			t.Setenv("SHELL", tt.shell)

			err := Shell(tt.envMap, nil, writer, tt.envPrefix, "")
			require.NoError(t, err, "Shell() error = %v", err)

			gotOut := writer.String()
			assert.Equal(t, tt.wantOut, gotOut, "Shell() output = %v, wantOut %v", gotOut, tt.wantOut)
		})
	}
}
