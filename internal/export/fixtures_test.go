package export

var exportFixtures = []struct {
	name         string
	envMap       map[string]string
	envSet       map[string]string
	unsetMap     []string
	envPrefix    string
	expectedFish string
	expectedBash string
}{
	{
		name: "basic",
		envMap: map[string]string{
			"KEY1": "VALUE1",
			"KEY2": "VALUE2",
		},
		expectedFish: `set --export KEY1 VALUE1;
set --export KEY2 VALUE2;
`,
		expectedBash: `export KEY1=VALUE1
export KEY2=VALUE2
`,
	},
	{
		name: "prefix",
		envMap: map[string]string{
			"KEY1": "VALUE1",
			"KEY2": "VALUE2",
		},
		envPrefix: "PREFIX_",
		expectedFish: `set --export PREFIX_KEY1 VALUE1;
set --export PREFIX_KEY2 VALUE2;
`,
		expectedBash: `export PREFIX_KEY1=VALUE1
export PREFIX_KEY2=VALUE2
`,
	},
	{
		name: "protected",
		envMap: map[string]string{
			"KEY1": "'",
			"KEY2": " ",
			"KEY3": `"`,
			"KEY4": "\\",
		},
		expectedFish: `set --export KEY1 ''"'"'';
set --export KEY2 ' ';
set --export KEY3 '"';
set --export KEY4 '\';
`,
		expectedBash: `export KEY1=''"'"''
export KEY2=' '
export KEY3='"'
export KEY4='\'
`,
	},
	{
		name: "erase_set",
		envSet: map[string]string{
			"PREFIX_KEY1": "VALUE1",
			"PREFIX_KEY2": "VALUE2",
		},
		envMap: map[string]string{
			"KEY1": "VALUE1",
			"KEY2": "VALUE2",
		},

		unsetMap:  []string{"KEY1"},
		envPrefix: "PREFIX_",
		expectedFish: `set --export PREFIX_KEY1 VALUE1;
set --export PREFIX_KEY2 VALUE2;
`,
		expectedBash: `export PREFIX_KEY1=VALUE1
export PREFIX_KEY2=VALUE2
`,
	},
	{
		name: "erase_not_set",
		envSet: map[string]string{
			"PREFIX_DANGLING": "DANGLING",
		},
		envMap: map[string]string{
			"PREFIX_KEY1": "VALUE1",
			"PREFIX_KEY2": "VALUE2",
		},

		unsetMap:  []string{"DANGLING"},
		envPrefix: "PREFIX_",
		expectedFish: `set --export PREFIX_PREFIX_KEY1 VALUE1;
set --export PREFIX_PREFIX_KEY2 VALUE2;
set --erase PREFIX_DANGLING;
`,
		expectedBash: `export PREFIX_PREFIX_KEY1=VALUE1
export PREFIX_PREFIX_KEY2=VALUE2
export PREFIX_DANGLING=
`,
	},
}
