package export

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// nolint:paralleltest
func TestFish(t *testing.T) {
	for _, tt := range exportFixtures {
		t.Run(tt.name, func(t *testing.T) {
			// Add any additional environment variables
			for k, v := range tt.envSet {
				t.Setenv(k, v)
			}

			var buf bytes.Buffer
			err := fish(tt.envMap, tt.unsetMap, &buf, tt.envPrefix)

			require.NoError(t, err)
			assert.Equal(t, tt.expectedFish, buf.String())
		})
	}
}
