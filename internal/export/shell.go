package export

import (
	"io"
	"os"
	"path"

	"github.com/rs/zerolog/log"
)

// Shell will export an environment map in a fish supported manner.
// If `shell` parameter is not specified it will default to $SHELL environment variable.
func Shell(envMap map[string]string, unsetMap []string, writer io.Writer, envPrefix string, shell string) error {
	if shell == "" {
		shell, _ = os.LookupEnv("SHELL")
	}

	shell = path.Base(shell)

	switch shell {
	case "sh":
		return bash(envMap, unsetMap, writer, envPrefix)
	case "bash":
		return bash(envMap, unsetMap, writer, envPrefix)
	case "zsh":
		return bash(envMap, unsetMap, writer, envPrefix)
	case "fish":
		return fish(envMap, unsetMap, writer, envPrefix)
	default:
		log.Warn().
			Str("shell", shell).
			Msg("⚠️  Warning: unrecognised shell. Assuming it can handle bash-like variable exports.")

		return bash(envMap, unsetMap, writer, envPrefix)
	}
}
