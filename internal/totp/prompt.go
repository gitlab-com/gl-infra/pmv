package totp

import (
	"errors"
	"fmt"

	pmverrors "gitlab.com/gitlab-com/gl-infra/pmv/internal/errors"
)

var errInvalidProvider = errors.New("invalid TOTP provider")

// GetProvider returns a TOTP Provider.
func GetProvider(id string) (TOTPProvider, error) {
	if id == "" {
		// legacy fallback if not specified
		id = "authenticator"
	}

	provider, ok := totpProviderLookup[id]
	if !ok {
		return nil, fmt.Errorf("invalid totp provider: %s: %w", id, errInvalidProvider)
	}

	err := provider.IsAvailable()
	if err != nil {
		return nil, fmt.Errorf("totp provider not available: %s: %w", id, errors.Join(errInvalidProvider, pmverrors.ErrSetup))
	}

	return provider, nil
}
