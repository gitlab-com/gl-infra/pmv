package totp

import (
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"

	"bitbucket.org/creachadair/shell"
	"github.com/bitfield/script"
	log "github.com/rs/zerolog/log"
)

type yubikeyTOTPProvider struct {
}

func (y *yubikeyTOTPProvider) ID() string {
	return "yubikey"
}

var ErrYubikeyNotAvailable = errors.New("yubikey TOTP is not available")

func (y *yubikeyTOTPProvider) IsAvailable() error {
	_, err := exec.LookPath("ykman")
	if err != nil {
		return fmt.Errorf("ykman not found on path: %w", ErrYubikeyNotAvailable)
	}

	yubikeyDeviceCount, err := script.Exec("ykman list").WithStderr(os.Stderr).CountLines()
	if err != nil {
		return fmt.Errorf("ykman list failed: %w", ErrYubikeyNotAvailable)
	}

	if yubikeyDeviceCount == 0 {
		return fmt.Errorf("no yubikey devices found: %w", ErrYubikeyNotAvailable)
	}

	if yubikeyDeviceCount > 1 && os.Getenv("PMV_YKMAN_OATH_DEVICE_SERIAL") == "" {
		log.Warn().Msg("Multiple Yubikeys detected. You may want to configure PMV_YKMAN_OATH_DEVICE_SERIAL with a specific key")
	}

	return nil
}

// lookup will check if the keys are available on the Yubikey,
// and return the first match found, if one is found.
func (y *yubikeyTOTPProvider) Lookup(mfaIdentifiers []string) (string, error) {
	args := []string{"ykman"}
	mfaSet := map[string]struct{}{}

	// Create a set
	for _, m := range mfaIdentifiers {
		mfaSet[m] = struct{}{}
	}

	// Get the serial number of the yubikey device to use.
	yubikeyDeviceSerial := os.Getenv("PMV_YKMAN_OATH_DEVICE_SERIAL")
	if yubikeyDeviceSerial != "" {
		// If the env var was set, extend args to support passing the serial.
		args = append(args, "--device", yubikeyDeviceSerial)
	}

	args = append(args, "oath", "accounts", "list")

	log.Debug().
		Strs("args", args).
		Msg("🔌 Checking for TOTP in Yubikey using ykman")

	found := ""

	err := script.Exec(shell.Join(args)).FilterScan(func(line string, writer io.Writer) {
		if found != "" {
			return
		}

		mfaID := strings.TrimSpace(line)
		_, ok := mfaSet[mfaID]
		if ok {
			found = mfaID
		}
	}).Wait()
	if err != nil {
		return found, fmt.Errorf("ykman oath accounts list failed: %w", err)
	}

	return found, nil
}

func (y *yubikeyTOTPProvider) Store(mfaIdentifier string, secretKey string) error {
	err := y.addAccount(mfaIdentifier, secretKey)
	if err != nil {
		return fmt.Errorf("failed to create Yubikey OATH account: %w", err)
	}

	return nil
}

func (y *yubikeyTOTPProvider) Retrieve(mfaIdentifier string) (string, error) {
	args := []string{"ykman"}

	// Get the serial number of the yubikey device to use.
	yubikeyDeviceSerial := os.Getenv("PMV_YKMAN_OATH_DEVICE_SERIAL")
	if yubikeyDeviceSerial != "" {
		// If the env var was set, extend args to support passing the serial.
		args = append(args, "--device", yubikeyDeviceSerial)
	}

	args = append(args, "oath", "accounts", "code", "--single", mfaIdentifier)

	log.Debug().
		Strs("args", args).
		Msg("🔌 Fetching MFA code using ykman")

	out, err := script.Exec(shell.Join(args)).WithStderr(os.Stderr).String()
	if err != nil {
		return "", fmt.Errorf("ykman oath accounts code failed: %w", err)
	}

	code := strings.TrimSpace(out)

	log.Debug().
		Str("code", code).
		Msg("ykman returned code")

	return code, nil
}

func (y *yubikeyTOTPProvider) addAccount(mfaIdentifier string, key string) error {
	args := []string{"ykman"}

	// Get the serial number of the yubikey device to use.
	yubikeyDeviceSerial := os.Getenv("PMV_YKMAN_OATH_DEVICE_SERIAL")
	if yubikeyDeviceSerial != "" {
		// If the env var was set, extend args to support passing the serial.
		args = append(args, "--device", yubikeyDeviceSerial)
	}

	args = append(args, "oath", "accounts", "add", "--touch", mfaIdentifier)

	log.Debug().
		Strs("args", args).
		Msg("🔌 Adding TOTP to Yubikey using ykman")

	_, err := script.Echo(key + "\n").Exec(shell.Join(args)).String()
	if err != nil {
		return fmt.Errorf("ykman oath account add failed: %w", err)
	}

	return nil
}
