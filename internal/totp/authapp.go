package totp

import (
	"bufio"
	"fmt"
	"net/url"
	"os"
	"strings"

	"github.com/charmbracelet/huh"
	"github.com/mdp/qrterminal"
	"github.com/rs/zerolog/log"
)

type authenticatorAppTOTPProvider struct {
}

func (a *authenticatorAppTOTPProvider) ID() string {
	return "authenticator"
}

func (a *authenticatorAppTOTPProvider) IsAvailable() error {
	return nil
}

func (a *authenticatorAppTOTPProvider) Lookup(mfaIdentifiers []string) (string, error) {
	log.Info().
		Strs("mfa_identifier", mfaIdentifiers).
		Msg("📲   ")

	options := []huh.Option[string]{}

	for _, v := range mfaIdentifiers {
		options = append(options, huh.NewOption(v, v))
	}

	options = append(options, huh.NewOption("None -- setup a new virtual MFA TOTP device for my Authenicator", ""))

	var id string

	_ = huh.NewSelect[string]().
		Title("The following virtual MFA TOTP devices are assigned to your AWS account.\n" +
			"Please choose one the one from your authenticator app that you would like to use.").
		Options(options...).
		Value(&id).
		Run()

	return id, nil
}

func (a *authenticatorAppTOTPProvider) Store(mfaIdentifier string, secretKey string) error {
	a.generateQRCode("pmv", mfaIdentifier, secretKey)

	var confirmed bool

	_ = huh.NewConfirm().
		Title("Have you captured this TOTP in your Authenticator app?").
		Affirmative("Yes").
		Negative("No").
		Value(&confirmed).
		Run()

	return nil
}

func (a *authenticatorAppTOTPProvider) Retrieve(mfaIdentifier string) (string, error) {
	reader := bufio.NewReader(os.Stdin)

	log.Info().
		Str("title", mfaIdentifier).
		Msg("📲 Use your Authenticator app to provide a token code")

	fmt.Fprintf(os.Stderr, "token: ") // This can't use the logger, as we don't want to write a newline

	tokenCode, err := reader.ReadString('\n')
	if err != nil {
		return "", fmt.Errorf("failed to read: %w", err)
	}

	tokenCode = strings.TrimSpace(tokenCode)

	return tokenCode, nil
}

func (a *authenticatorAppTOTPProvider) generateQRCode(title string, issuer string, secret string) {
	u := url.URL{}
	u.Scheme = "otpauth"
	u.Host = "totp"
	u.Path = title
	values := u.Query()
	values.Add("secret", secret)
	values.Add("issuer", issuer)
	u.RawQuery = values.Encode()

	log.Info().Msg("📲 OK, time to setup a virtual MFA using your Authenticator app")

	qrterminal.GenerateHalfBlock(u.String(), qrterminal.L, os.Stderr)
}
