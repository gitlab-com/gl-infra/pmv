package totp

import (
	"errors"
)

// TOTPProvider interfacer provides a way of interacting with TOTP Providers.
type TOTPProvider interface {
	ID() string
	IsAvailable() error
	Lookup(mfaIdentifier []string) (string, error)
	Store(mfaIdentifier string, secretKey string) error
	Retrieve(mfaIdentifier string) (string, error)
}

var totpProviderLookup = map[string]TOTPProvider{
	"yubikey":       &yubikeyTOTPProvider{},
	"authenticator": &authenticatorAppTOTPProvider{},
}

var ErrTOTPFailed = errors.New("TOTP failed")
