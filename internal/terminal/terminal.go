package terminal

import (
	"errors"
	"os"

	log "github.com/rs/zerolog/log"

	shellescape "al.essio.dev/pkg/shellescape"
	"golang.org/x/term"
)

var errTerminalOutput = errors.New("terminal output")

// CheckForTerminal checks whether pmv is connected to a terminal, which probably
// means the user has run it without eval.
func CheckForTerminal(allowBypass bool) error {
	if term.IsTerminal(int(os.Stdout.Fd())) { //nolint:gosec
		if allowBypass {
			log.Info().Msg("⚠️  Connected to terminal, but bypass set, allowing.")
			log.Info().Msg("⚠️  SECRETS TO FOLLOW ------------------------------")

			return nil
		}

		log.Info().Msg("⚠️  Did you mean to invoke?")
		log.Info().Msgf("⚠️  `eval $(%s)`", shellescape.QuoteCommand(os.Args))
		log.Info().Msg("⚠️  If you know what you're doing an intended to do this, add the `--allow-terminal` flag and try again.")

		return errTerminalOutput
	}

	return nil
}
