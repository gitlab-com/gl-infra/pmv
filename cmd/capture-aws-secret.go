package cmd

import (
	"encoding/json"
	"fmt"

	"github.com/rs/zerolog/log"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/aws"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/op"
)

var secretID string

var captureAWSSecret = &cobra.Command{
	Use:          "aws-secret",
	ValidArgs:    []string{"tags"},
	Short:        "Capture an AWS Secret into 1password",
	Long:         `Capture an AWS Secret into 1password`,
	Args:         cobra.ExactArgs(0),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if secretID == "" {
			return fmt.Errorf("--secret-id required: %w", errInvalidArgument)
		}

		if title == "" {
			return fmt.Errorf("--title required: %w", errInvalidArgument)
		}

		whoami, err := op.WhoAmI()
		if err != nil {
			return fmt.Errorf("whoami failed: %w", err)
		}

		log.Info().Msg("🔓 retrieving secret from AWS...")

		secretBody, err := aws.RetrieveSecret(secretID)
		if err != nil {
			log.Error().
				Err(err).
				Msg("❌ unable to retrieve secret.")

			return fmt.Errorf("unable to retrieve secret: %w", err)
		}

		var secretValues map[string]interface{}
		err = json.Unmarshal([]byte(secretBody), &secretValues)
		if err != nil {
			return fmt.Errorf("failed to unmarshal body: %w", err)
		}

		if deleteTaggedItems {
			log.Info().
				Str("tags", tags).
				Msg("🗑  Deleting old items")

			err = op.DeleteItemsByTag(tags, vault)
			if err != nil {
				return fmt.Errorf("delete failed: %w", err)
			}
		}

		secretValueIntf, ok := secretValues["Value"]
		if !ok {
			return fmt.Errorf("secret must contain a Value field: %w", errInvalidArgument)
		}

		secretValue, ok := secretValueIntf.(string)
		if !ok {
			return fmt.Errorf("secret value must be a string: %w", errInvalidArgument)
		}

		passwordField := &model.OPField{
			ID:    "390460F645FA4B25A5B91D2AD980D546", // Arbitrary UUID
			Type:  "CONCEALED",
			Label: "Password",
			Value: secretValue,
		}

		item := &model.OPItem{}
		item.Category = opLoginCategory

		item.Fields = []*model.OPField{
			passwordField,
		}

		returnedItem, err := op.CreateItem(title, item, "", tags, vault)
		if err != nil {
			return fmt.Errorf("failed to create new item: %w", err)
		}

		log.Info().
			Str("link", returnedItem.ShareLink(whoami.AccountUUID)).
			Msgf("🐣 Created item")

		return nil
	},
}

func init() {
	captureCmd.AddCommand(captureAWSSecret)
	captureAWSSecret.PersistentFlags().StringVar(&secretID, "secret-id", "", "SecretID to retrieve from AWS Secrets Manager")
}
