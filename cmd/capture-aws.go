package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	log "github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/secureentry"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/totp"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/aws"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/op"
)

const awsMFAArnKey = "AWS Virtual MFA Arn"

var toptProviderFlag = ""

var captureAwsCmd = &cobra.Command{
	Use:          "aws",
	ValidArgs:    []string{"tags"},
	Short:        "Capture AWS credentials in 1password",
	Long:         `Capture AWS credentials in 1password`,
	Args:         cobra.ExactArgs(0),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		reader := bufio.NewReader(os.Stdin)

		// Initialize this early to allow for fast failure
		var toptProvider totp.TOTPProvider
		var err error

		whoami, err := op.WhoAmI()
		if err != nil {
			return fmt.Errorf("whoami failed: %w", err)
		}

		if !skipMFA {
			toptProvider, err = totp.GetProvider(toptProviderFlag)
			if err != nil {
				return fmt.Errorf("totp provider unavailable: %w", err)
			}
		}

		fmt.Print("pmv: 🔓 Enter AWS_ACCESS_KEY: ")
		accessKey, err := reader.ReadString('\n')
		if err != nil {
			return fmt.Errorf("failed to read: %w", err)
		}

		fmt.Print("pmv: 🔓 Enter AWS_SECRET_ACCESS_KEY: ")
		secretAccessKey, err := secureentry.SecureRead()
		if err != nil {
			return fmt.Errorf("failed to read secret: %w", err)
		}

		log.Info().
			Msg("🔓 Thanks. Verifying the credentials with AWS...")

		accessKey = strings.TrimSpace(accessKey)

		username, awsAccountID, accountAlias, err := aws.VerifyCallerIdentity(accessKey, secretAccessKey, "")
		if err != nil {
			log.Error().
				Err(err).
				Msg("❌ verification failed")

			return fmt.Errorf("credential verification failed: %w", err)
		}

		log.Info().
			Str("username", username).
			Err(err).
			Msg("✅ Verification passed. Greetings! 👋")

		// Auto-create a title
		if title == "" {
			title = generateTitle(accountAlias, username, awsAccountID)
		}

		var mfaFields []*model.OPField

		if !skipMFA {
			mfaFields, err = aws.CreateMFA(accessKey, secretAccessKey, username, toptProvider, reader)
			if err != nil {
				log.Error().
					Err(err).
					Msg("❌ MFA setup failed")

				return fmt.Errorf("credential verification failed: %w", err)
			}
		}

		if deleteTaggedItems {
			log.Info().
				Str("tags", tags).
				Msg("🗑  Deleting old items")

			err = op.DeleteItemsByTag(tags, vault)
			if err != nil {
				return fmt.Errorf("delete failed: %w", err)
			}
		}

		awsAccessKeyField := &model.OPField{
			ID:    "A97C500242C24A9B810AA7CA1234C2A6", // Arbitrary UUID
			Type:  "STRING",
			Label: "env:AWS_ACCESS_KEY",
			Value: accessKey,
		}

		awsAccessKeyIDField := &model.OPField{
			ID:    "737DFFD1001641D9B23B6620B85D4FF1", // Arbitrary UUID
			Type:  "STRING",
			Label: "env:AWS_ACCESS_KEY_ID",
			Value: accessKey,
		}

		awsAccountIDField := &model.OPField{
			ID:    "187DFFD1001641D9B23B6620B85D4D8", // Arbitrary UUID
			Type:  "STRING",
			Label: "AWS Account ID",
			Value: awsAccountID,
		}

		awsSecretAccessKeyField := &model.OPField{
			ID:    "390460F645FA4B25A5B91D2AD980D546", // Arbitrary UUID
			Type:  "CONCEALED",
			Label: "env:AWS_SECRET_ACCESS_KEY",
			Value: secretAccessKey,
		}

		awsConsoleLoginURL := fmt.Sprintf(`https://%s.signin.aws.amazon.com/console`, awsAccountID)
		item := &model.OPItem{}
		item.Category = opLoginCategory

		item.Fields = []*model.OPField{
			awsAccountIDField,
			awsAccessKeyField,
			awsAccessKeyIDField,
			awsSecretAccessKeyField,
		}

		if tags != "" {
			pmvCommandField := &model.OPField{
				ID:    "1234FFD1001641D9B23B6620B85D555", // Arbitrary UUID
				Type:  "string",
				Label: "PMV Command",
				Value: fmt.Sprintf("eval $(pmv env '%s')", tags),
			}
			item.Fields = append(item.Fields, pmvCommandField)
		}

		item.Fields = append(item.Fields, mfaFields...)

		returnedItem, err := op.CreateItem(title, item, awsConsoleLoginURL, tags, vault)
		if err != nil {
			return fmt.Errorf("failed to create new item in op: %w", err)
		}

		log.Info().
			Str("link", returnedItem.ShareLink(whoami.AccountUUID)).
			Msg("🐣 Created item")

		return nil
	},
}

func generateTitle(accountAlias string, username string, awsAccountID string) string {
	titleParts := []string{"AWS Access"}

	if description != "" {
		titleParts = append(titleParts, description)
	}

	if accountAlias != "" {
		titleParts = append(titleParts, accountAlias)
	}

	if username != "" {
		titleParts = append(titleParts, username)
	}

	titleParts = append(titleParts, fmt.Sprintf("Account ID %s", awsAccountID))

	return strings.Join(titleParts, ": ")
}

func init() {
	captureCmd.AddCommand(captureAwsCmd)
	captureAwsCmd.PersistentFlags().StringVarP(&toptProviderFlag, "totp", "", "yubikey", "TOTP storage - yubikey or authenticator. Defaults to yubikey")
}
