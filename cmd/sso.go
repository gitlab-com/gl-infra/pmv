package cmd

import (
	"github.com/spf13/cobra"
)

var ssoCmd = &cobra.Command{
	Use:       "sso",
	ValidArgs: []string{},
	Short:     "Perform SSO login",
	Long:      `Perform SSO login`,
}

func init() {
	rootCmd.AddCommand(ssoCmd)
}
