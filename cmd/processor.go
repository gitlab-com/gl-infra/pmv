package cmd

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/ui"

	"github.com/rs/zerolog/log"

	shellescape "al.essio.dev/pkg/shellescape"

	"github.com/bitfield/script"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"
)

var vault string
var errIncorrectNumber = errors.New("unexpected number of items found")

type itemProcessor interface {
	item(title string, key string, value string) error
	done() error
}

func processItem(jsonItem string, processor itemProcessor, modelHandler func(*model.OPItem)) error {
	item, err := model.ParseItem(jsonItem)
	if err != nil {
		return fmt.Errorf("parse failed: %w", err)
	}

	modelHandler(item)

	log.Info().
		Str("title", item.Title).
		Msg("🧩 Loaded item")

	for _, field := range item.Fields {
		label := field.Label
		value := field.Value

		titleParts := strings.Split(label, ",")
		for _, part := range titleParts {
			err = processor.item(item.Title, part, value)
			if err != nil {
				return fmt.Errorf("item processing failed: %w", err)
			}
		}
	}

	return nil
}

func defaultModelHandler(*model.OPItem) {}

func run(tag string, vault string, processor itemProcessor, modelHandler func(*model.OPItem)) error {
	l := log.Info().
		Str("tag", tag)

	if vault != "" {
		l = l.Str("vault", vault)
	}

	l.Msg("🔓 Fetching 1password items")

	output, err := executeOpItemGet(tag, vault)
	if err != nil {
		return fmt.Errorf("op invocation failed: %w", err)
	}

	count := iterateOverOpOutput(output, processor, modelHandler)

	err = checkResultCounts(tag, count)
	if err != nil {
		return err
	}

	err = processor.done()
	if err != nil {
		return fmt.Errorf("failed to process op output: %w", err)
	}

	log.Info().
		Msg("🔓 Complete, loaded items...")

	return nil
}

func iterateOverOpOutput(output string, processor itemProcessor, modelHandler func(*model.OPItem)) int {
	count := 0

	// op item get uses a horrible concatenated JSON format that's not good for anything
	// we need to break each concatenated part up
	scanner := bufio.NewScanner(strings.NewReader(output))
	for jsonChunk := ""; scanner.Scan(); {
		line := scanner.Text()
		if jsonChunk == "" {
			jsonChunk = line
		} else {
			jsonChunk = jsonChunk + "\n" + line
		}

		if line == "}" {
			err := processItem(jsonChunk, processor, modelHandler)
			if err != nil {
				log.Warn().
					Str("chunk", jsonChunk).
					Err(err).
					Msg("🔓 ignoring JSON")
			}

			jsonChunk = "" // start processing next chunk
			count = count + 1
		}
	}

	return count
}

func executeOpItemGet(tag string, vault string) (string, error) {
	command := []string{
		"op", "item", "list",
		"--tags",
		tag,
		"--cache",
		"--format=json",
	}

	if vault != "" {
		command = append(command, "--vault", vault)
	}

	commandFull := shellescape.QuoteCommand(command)

	listOutput, err := ui.Spin("running "+commandFull, func() (string, error) {
		return runCommandWithString(commandFull)
	})
	if err != nil {
		return "", err // nolint:wrapcheck
	}

	listOutput = strings.TrimSpace(listOutput)

	if listOutput == "[]" {
		log.Debug().
			Msg("op item list returned zero items, skipping pipe")

		return "", nil
	}

	opItemGet := "op item get - --cache --format=json"

	output, err := ui.Spin("running "+opItemGet, func() (string, error) {
		log.Debug().
			Str("cmd", opItemGet).
			Msg("exec")

		pipe := script.Echo(listOutput).
			Exec(opItemGet)

		if e := log.Debug(); e.Enabled() {
			// In debug mode we log stderr to stderr
			pipe.WithStderr(os.Stderr)
		}

		output, err2 := pipe.String()
		if err2 != nil {
			return "", fmt.Errorf("op item get failed: %w", err)
		}

		return output, nil
	})
	if err != nil {
		return "", err //nolint:wrapcheck
	}

	return output, nil
}

func runCommandWithString(commandFull string) (string, error) {
	log.Debug().
		Str("cmd", commandFull).
		Msg("exec")

	pipe := script.Exec(commandFull)

	if e := log.Debug(); e.Enabled() {
		// In debug mode we log stderr to stderr
		pipe.WithStderr(os.Stderr)
	}

	listOutput, err := pipe.String()
	if err != nil {
		return "", fmt.Errorf("op item list failed: %w", err)
	}

	return listOutput, nil
}

func checkResultCounts(tag string, count int) error {
	if !allowMultiple && count > 1 {
		return fmt.Errorf("op returned %d items for tag %s, expected one. Please double-check the tag and vault. %w", count, tag, errIncorrectNumber)
	}

	if !allowNone && count == 0 {
		return fmt.Errorf("op returned no items for tag %s, expected one. Please double-check the tag and vault. %w", tag, errIncorrectNumber)
	}

	if count == 0 {
		log.Warn().
			Msg("no matching items found")
	}

	return nil
}

func fetchTag(tag string) (map[string]string, *model.OPItem, error) {
	var opItem *model.OPItem

	processor := &environmentProcessor{
		writer: nil,
		envMap: make(map[string]string),
	}

	modelHandler := func(m *model.OPItem) {
		opItem = m
	}

	allowTerminalBypass = true

	if err := run(tag, vault, processor, modelHandler); err != nil {
		return nil, opItem, fmt.Errorf("env failed: %w", err)
	}

	return processor.envMap, opItem, nil
}
