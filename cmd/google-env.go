package cmd

import (
	"fmt"
	"os"
	"time"

	log "github.com/rs/zerolog/log"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/export"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/google"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/terminal"
)

var googleEnvCmd = &cobra.Command{
	Use:          "env",
	ValidArgs:    []string{},
	Short:        "Performs a Google OAuth2 authentication and exports the credentials to the environment",
	Long:         `Performs a Google OAuth2 authentication and exports the credentials to the environment`,
	Args:         cobra.ExactArgs(0),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if terminal.CheckForTerminal(allowTerminalBypass) != nil {
			os.Exit(3)
		}

		token, err := google.PerformOAuth2Authentication(cmd.Context(), organization)
		if err != nil {
			return fmt.Errorf("google oauth2 failed: %w", err)
		}

		envMap := map[string]string{
			"GOOGLE_APPLICATION_CREDENTIALS": "/dev/null",
			"CLOUDSDK_AUTH_ACCESS_TOKEN":     token.AccessToken,
			"GOOGLE_OAUTH_ACCESS_TOKEN":      token.AccessToken,
			"GOOGLE_OAUTH_REFRESH_TOKEN":     token.RefreshToken,
			"GOOGLE_OAUTH_TOKEN_TYPE":        token.TokenType,
			"GOOGLE_TOKEN_EXPIRATION":        token.Expiry.UTC().Format(time.RFC3339),
		}

		err = export.Shell(envMap, nil, cmd.OutOrStdout(), envPrefix, specifiedShell)
		if err != nil {
			return fmt.Errorf("failed to write exported: %w", err)
		}

		log.Info().
			Msg("🔓 Complete, authentication configured...")

		return nil
	},
}

func init() {
	googleCmd.AddCommand(googleEnvCmd)
	googleEnvCmd.PersistentFlags().BoolVar(&allowTerminalBypass, "allow-terminal", false, "Allow output to terminal")
	googleEnvCmd.PersistentFlags().StringVarP(&envPrefix, "prefix", "p", "", "Prefix to add to environment variables")
	googleEnvCmd.PersistentFlags().StringVarP(&specifiedShell, "shell", "s", "", "Create commands for a specific shell, defaults to $SHELL")
}
