package cmd

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/op"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/gitlab"

	"github.com/rs/zerolog/log"

	"bitbucket.org/creachadair/shell"
	"github.com/bitfield/script"
	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"
)

var errRotateGitLab = errors.New("gitlab token rotation failed")

var expiresAt string
var with string

var rotateGitLab = &cobra.Command{
	Use:          "gitlab",
	ValidArgs:    []string{"tags"},
	Short:        "Rotate GitLab credentials in 1password",
	Long:         `Rotate GitLab credentials in 1password`,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		tag := args[0]

		whoami, err := op.WhoAmI()
		if err != nil {
			return fmt.Errorf("whoami failed: %w", err)
		}

		envMap, opItem, err := fetchTag(tag)
		if err != nil {
			return fmt.Errorf("env failed: %w", err)
		}

		gitlabToken, ok := envMap["GITLAB_TOKEN"]
		if !ok {
			gitlabToken = ""
		}

		if gitlabInstanceEnv, ok := envMap["GITLAB_INSTANCE"]; ok {
			gitlabInstance = gitlabInstanceEnv
		}

		log.Info().
			Str("instance", gitlabInstance).
			Msg("🔑  Rotating token in GitLab...")

		writeToken := gitlabToken
		if with != "" {
			writeTokenEnvMap, _, fechErr := fetchTag(with)
			if fechErr != nil {
				return fmt.Errorf("failed to fetch tag %s: %w", with, fechErr)
			}

			token, ok := writeTokenEnvMap["GITLAB_TOKEN"]
			if !ok {
				log.Warn().Msg("⛔️  Write token not found in 1password")
			} else {
				writeToken = token
			}
		}

		newToken, newExpiresAt, err := gitlab.RotatePersonalAccessToken(gitlabInstance, expiresAt, gitlabToken, writeToken)
		if err != nil {
			return errors.Join(err, errRotateGitLab)
		}

		log.Info().
			Str("expires_at", newExpiresAt).
			Msg("🔑  New token expiry date...")

		err = updateRotatedGitLabItem(opItem, newToken)
		if err != nil {
			return fmt.Errorf("failed to update op item, aborting: %w: %w", err, errRotateGitLab)
		}

		jsonString, err := json.Marshal(&opItem)
		if err != nil {
			return fmt.Errorf("failed to marshall secret: %w", err)
		}

		log.Info().
			Msg("🐣  Creating new 1password item...")

		createCmdArgs := []string{"op", "item", "create", "-", "--format=json"}
		createCmdArgs = append(createCmdArgs, "--vault", opItem.Vault.ID)
		createCmdArgs = append(createCmdArgs, "--title", opItem.Title)

		output, err := script.Echo(string(jsonString)).Exec(shell.Join(createCmdArgs)).String()
		if err != nil {
			return fmt.Errorf("op create item Login: %w", err)
		}

		newItem, err := model.ParseItem(output)
		if err != nil {
			return fmt.Errorf("failed to parse response: %w", err)
		}

		log.Info().
			Str("link", newItem.ShareLink(whoami.AccountUUID)).
			Msg("🐣 Created item")

		log.Info().
			Msg("🗑️  Deleting old 1password item.")

		deleteCmdArgs := []string{"op", "item", "delete", opItem.ID}
		deleteCmdArgs = append(deleteCmdArgs, "--vault", opItem.Vault.ID)

		_, err = script.Exec(shell.Join(deleteCmdArgs)).String()
		if err != nil {
			return fmt.Errorf("failed to delete old item: %w", err)
		}

		log.Info().
			Msg("🚀 Rotation complete...")

		return nil
	},
}

func updateRotatedGitLabItem(opItem *model.OPItem, newGitLabToken string) error {
	setToken := false

	updatedFields := make([]*model.OPField, 0, len(opItem.Fields))

	for _, field := range opItem.Fields {
		// Exclude certain fields that don't have a value
		// as 1password seems to have a bug that duplicates
		// them.
		if excludeField(field) {
			continue
		}

		label := field.Label

		if label == "env:GITLAB_TOKEN" {
			field.Value = newGitLabToken
			setToken = true
		}

		updatedFields = append(updatedFields, field)
	}

	if !setToken {
		return fmt.Errorf("failed to find GitLab Token fields, aborting: %w", errRotateGitLab)
	}

	opItem.Fields = updatedFields

	return nil
}

func init() {
	rotateCmd.AddCommand(rotateGitLab)
	rotateGitLab.PersistentFlags().StringVar(&gitlabInstance, "gitlab-instance", "https://gitlab.com", "GitLab instance to use")
	rotateGitLab.PersistentFlags().StringVar(&expiresAt, "expires", "", "Expiry as a date in YYYY-MM-DD format, or a duration (eg 30d)")
	rotateGitLab.PersistentFlags().StringVar(&with, "with", "", "A tag identifing the token to use to rotate (for rotating tokens without the api scope)")
}
