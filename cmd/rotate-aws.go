package cmd

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/op"

	"github.com/rs/zerolog/log"

	"bitbucket.org/creachadair/shell"
	"github.com/bitfield/script"
	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/aws"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"
)

var errRotateAWS = errors.New("aws rotation failed")
var inactivateOldKey bool

var rotateAWS = &cobra.Command{
	Use:          "aws",
	ValidArgs:    []string{"tags"},
	Short:        "Rotate AWS credentials in 1password",
	Long:         `Rotate AWS credentials in 1password`,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		tag := args[0]

		whoami, err := op.WhoAmI()
		if err != nil {
			return fmt.Errorf("whoami failed: %w", err)
		}

		processor := &environmentProcessor{
			writer: nil,
			envMap: make(map[string]string),
		}

		var opItem *model.OPItem

		modelHandler := func(m *model.OPItem) {
			opItem = m
		}

		allowTerminalBypass = true

		err = run(tag, vault, processor, modelHandler)
		if err != nil {
			return fmt.Errorf("env failed: %w", err)
		}

		accessKey, ok := processor.envMap["AWS_ACCESS_KEY"]
		if !ok {
			accessKey = ""
		}

		secretAccessKey, ok := processor.envMap["AWS_SECRET_ACCESS_KEY"]
		if !ok {
			secretAccessKey = ""
		}

		sessionToken, ok := processor.envMap["AWS_SESSION_TOKEN"]
		if !ok {
			sessionToken = ""
		}

		log.Info().
			Msg("🔑  Creating new access key in AWS...")

		newAccessKey, newSecretAccessKey, err := aws.CreateNewAccessKey(accessKey, secretAccessKey, sessionToken)
		if err != nil {
			return errors.Join(err, errRotateAWS)
		}

		err = updateItem(opItem, newAccessKey, newSecretAccessKey)

		if err != nil {
			return fmt.Errorf("failed to update op item, aborting: %w: %w", err, errRotateAWS)
		}

		jsonString, err := json.Marshal(&opItem)
		if err != nil {
			return fmt.Errorf("failed to marshall secret: %w", err)
		}

		log.Info().
			Msg("🐣  Creating new 1password item...")

		createCmdArgs := []string{"op", "item", "create", "-", "--format=json"}
		createCmdArgs = append(createCmdArgs, "--url", opItem.URLs[0].HRef)
		createCmdArgs = append(createCmdArgs, "--vault", opItem.Vault.ID)
		createCmdArgs = append(createCmdArgs, "--title", opItem.Title)

		output, err := script.Echo(string(jsonString)).Exec(shell.Join(createCmdArgs)).String()
		if err != nil {
			return fmt.Errorf("op create item Login: %w", err)
		}

		if inactivateOldKey {
			log.Warn().
				Msg("😵  Inactivating old key in AWS...")

			err = aws.DisableAccessKey(accessKey, secretAccessKey, sessionToken)
			if err != nil {
				return fmt.Errorf("failed to disable old access key: %w", err)
			}
		} else {
			log.Warn().
				Msg("❌  Deleting old key in AWS...")

			err = aws.DeleteAccessKey(accessKey, secretAccessKey, sessionToken)
			if err != nil {
				return fmt.Errorf("failed to delete old access key: %w", err)
			}
		}

		newItem, err := model.ParseItem(output)
		if err != nil {
			return fmt.Errorf("failed to parse response: %w", err)
		}

		log.Info().
			Str("link", newItem.ShareLink(whoami.AccountUUID)).
			Msg("🐣 Created item")

		log.Info().
			Msg("🗑️  Deleting old 1password item.")

		deleteCmdArgs := []string{"op", "item", "delete", opItem.ID}
		deleteCmdArgs = append(deleteCmdArgs, "--vault", opItem.Vault.ID)

		_, err = script.Exec(shell.Join(deleteCmdArgs)).String()
		if err != nil {
			return fmt.Errorf("failed to delete old item: %w", err)
		}

		log.Info().
			Msg("🚀 Rotation complete...")

		return nil
	},
}

func excludeField(field *model.OPField) bool {
	return (field.ID == "notesPlain" || field.ID == "username" || field.ID == "password") && field.Value == ""
}

func updateItem(opItem *model.OPItem, newAccessKey string, newSecretAccessKey string) error {
	setAccessKey := false
	setSecretAccessKey := false

	updatedFields := make([]*model.OPField, 0, len(opItem.Fields))

	for _, field := range opItem.Fields {
		// Exclude certain fields that don't have a value
		// as 1password seems to have a bug that duplicates
		// them.
		if excludeField(field) {
			continue
		}

		label := field.Label

		if label == "env:AWS_ACCESS_KEY" || label == "env:AWS_ACCESS_KEY_ID" {
			field.Value = newAccessKey
			setAccessKey = true
		}

		if label == "env:AWS_SECRET_ACCESS_KEY" {
			field.Value = newSecretAccessKey
			setSecretAccessKey = true
		}

		updatedFields = append(updatedFields, field)
	}

	if !setAccessKey {
		return fmt.Errorf("failed to find Access Key fields, aborting: %w", errRotateAWS)
	}

	if !setSecretAccessKey {
		return fmt.Errorf("failed to find Secret Access Key fields, aborting: %w", errRotateAWS)
	}

	opItem.Fields = updatedFields

	return nil
}

func init() {
	rotateCmd.AddCommand(rotateAWS)
	rotateAWS.PersistentFlags().BoolVarP(&inactivateOldKey, "inactivate-old-key", "i", false, "Inactivate old key instead of deleting it.")
}
