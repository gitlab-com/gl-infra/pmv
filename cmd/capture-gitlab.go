package cmd

import (
	"fmt"
	"strings"

	"github.com/rs/zerolog/log"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/secureentry"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/gitlab"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/model"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/op"
)

var gitlabInstance = "https://gitlab.com/"

var captureGitLabCmd = &cobra.Command{
	Use:          "gitlab",
	ValidArgs:    []string{"tags"},
	Short:        "Capture GitLab token in 1password",
	Long:         `Capture GitLab token in 1password`,
	Args:         cobra.ExactArgs(0),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		whoami, err := op.WhoAmI()
		if err != nil {
			return fmt.Errorf("whoami failed: %w", err)
		}

		fmt.Print("pmv: 🔓 Enter GITLAB_TOKEN: ")
		gitLabToken, err := secureentry.SecureRead()
		if err != nil {
			return fmt.Errorf("failed to read secret: %w", err)
		}

		log.Info().
			Str("url", gitlabInstance).
			Msg("🔓 Thanks. Verifying the credentials with GitLab")

		username, err := gitlab.VerifyToken(gitlabInstance, gitLabToken)
		if err != nil {
			log.Error().
				Err(err).
				Msg("❌ verification failed")

			return fmt.Errorf("credential verification failed: %w", err)
		}

		log.Info().
			Str("username", username).
			Msg("✅ Verification passed")

		// Auto-create a title
		if title == "" {
			title = generateGitLabItemTitle(gitlabInstance, description, username)
		}

		if deleteTaggedItems {
			log.Info().
				Str("tags", tags).
				Msg("🗑  Deleting old items")

			err = op.DeleteItemsByTag(tags, vault)
			if err != nil {
				return fmt.Errorf("delete failed: %w", err)
			}
		}

		gitlabUsernameField := &model.OPField{
			ID:    "DCA32FE2D688465B806A07998EF5F905", // Arbitrary UUID
			Type:  "STRING",
			Label: "env:GITLAB_USERNAME",
			Value: username,
		}

		gitlabTokenField := &model.OPField{
			ID:    "390460F645FA4B25A5B91D2AD980D546", // Arbitrary UUID
			Type:  "CONCEALED",
			Label: "env:GITLAB_TOKEN",
			Value: gitLabToken,
		}

		gitlabInstanceField := &model.OPField{
			ID:    "B751A8E89FB44C1BAE0DF0FD27645FB7", // Arbitrary UUID
			Type:  "STRING",
			Label: "env:GITLAB_INSTANCE",
			Value: gitlabInstance,
		}

		item := &model.OPItem{}
		item.Category = opLoginCategory

		item.Fields = []*model.OPField{
			gitlabUsernameField,
			gitlabTokenField,
			gitlabInstanceField,
		}

		if tags != "" {
			pmvCommandField := &model.OPField{
				ID:    "1234FFD1001641D9B23B6620B85D555", // Arbitrary UUID
				Type:  "string",
				Label: "PMV Command",
				Value: fmt.Sprintf("eval $(pmv env '%s')", tags),
			}
			item.Fields = append(item.Fields, pmvCommandField)
		}

		returnedItem, err := op.CreateItem(title, item, "", tags, vault)
		if err != nil {
			return fmt.Errorf("failed to create new item: %w", err)
		}

		log.Info().
			Str("link", returnedItem.ShareLink(whoami.AccountUUID)).
			Msg("🐣 Created item")

		return nil
	},
}

func generateGitLabItemTitle(username string, description string, gitlabInstance string) string {
	titleParts := []string{"GitLab Access Token"}

	if description != "" {
		titleParts = append(titleParts, description)
	}

	if username != "" {
		titleParts = append(titleParts, username)
	}

	titleParts = append(titleParts, gitlabInstance)

	return strings.Join(titleParts, ": ")
}

func init() {
	captureCmd.AddCommand(captureGitLabCmd)
	captureGitLabCmd.PersistentFlags().StringVar(&gitlabInstance, "gitlab-instance", "https://gitlab.com", "GitLab instance to use")
}
