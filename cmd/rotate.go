package cmd

import (
	"github.com/spf13/cobra"
)

var rotateCmd = &cobra.Command{
	Use:       "rotate",
	ValidArgs: []string{"tags"},
	Short:     "Rotate credentials in 1password",
	Long:      `Rotate credentials in 1password`,
}

func init() {
	rootCmd.AddCommand(rotateCmd)
}
