package cmd

import (
	"github.com/spf13/cobra"
)

var gitlabCmd = &cobra.Command{
	Use:       "gitlab",
	ValidArgs: []string{},
	Short:     "Perform GitLab login",
	Long:      `Perform GitLab login`,
}

func init() {
	rootCmd.AddCommand(gitlabCmd)
}
