package cmd

import (
	"fmt"
	"os"
	"time"

	log "github.com/rs/zerolog/log"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/aws"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/export"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/terminal"
)

var (
	startURL      string
	accountID     string
	roleName      string
	assumeRoleARN []string
)

var ssoEnvCmd = &cobra.Command{
	Use:          "env",
	ValidArgs:    []string{},
	Short:        "Export SSO credentials to environment",
	Long:         `Export SSO credentials to environment`,
	Args:         cobra.ExactArgs(0),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if terminal.CheckForTerminal(allowTerminalBypass) != nil {
			os.Exit(3)
		}

		creds, err := aws.PerformSSOAuthentication(startURL, accountID, roleName, assumeRoleARN)
		if err != nil {
			return fmt.Errorf("sso failed: %w", err)
		}

		expirationDate := ""
		if creds.Expiration != nil {
			expirationDate = creds.Expiration.UTC().Format(time.RFC3339)
		}

		envMap := map[string]string{
			"AWS_SESSION_TOKEN":      *creds.SessionToken,
			"AWS_ACCESS_KEY":         *creds.AccessKeyId,
			"AWS_ACCESS_KEY_ID":      *creds.AccessKeyId,
			"AWS_SECRET_ACCESS_KEY":  *creds.SecretAccessKey,
			"AWS_SESSION_EXPIRATION": expirationDate,
		}

		err = export.Shell(envMap, nil, cmd.OutOrStdout(), envPrefix, specifiedShell)
		if err != nil {
			return fmt.Errorf("failed to write exported: %w", err)
		}

		log.Info().Msg("🔓 Complete, authentication configured...")

		return nil
	},
}

func init() {
	ssoCmd.AddCommand(ssoEnvCmd)
	ssoEnvCmd.PersistentFlags().BoolVar(&allowTerminalBypass, "allow-terminal", false, "Allow output to terminal")
	ssoEnvCmd.PersistentFlags().StringVarP(&envPrefix, "prefix", "p", "", "Prefix to add to environment variables")

	ssoEnvCmd.PersistentFlags().StringVarP(&startURL, "start-url", "s", "", "AWS Identity Center start URL, eg `https://d-9067908776.awsapps.com/start#`")
	ssoEnvCmd.PersistentFlags().StringVarP(&specifiedShell, "shell", "", "", "Create commands for a specific shell, defaults to $SHELL")

	if err := ssoEnvCmd.MarkPersistentFlagRequired("start-url"); err != nil {
		panic(err)
	}

	ssoEnvCmd.PersistentFlags().StringVarP(&accountID, "account-id", "a", "", "AWS Account ID you wish to log into")

	if err := ssoEnvCmd.MarkPersistentFlagRequired("account-id"); err != nil {
		panic(err)
	}

	ssoEnvCmd.PersistentFlags().StringVarP(&roleName, "role-name", "r", "", "AWS Role within the account you wish to log into, eg `AdministratorAccess`")

	if err := ssoEnvCmd.MarkPersistentFlagRequired("role-name"); err != nil {
		panic(err)
	}

	ssoEnvCmd.PersistentFlags().StringArrayVarP(&assumeRoleARN, "assume-role-arn", "R", nil,
		"Once initial login has been completed, perform an AssumeRole into a new account, multiple calls can be used to  traverse multiple roles")
}
