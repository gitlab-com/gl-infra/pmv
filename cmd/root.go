package cmd

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/charmbracelet/lipgloss"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	pmverrors "gitlab.com/gitlab-com/gl-infra/pmv/internal/errors"

	"github.com/spf13/cobra"
)

var version = "DEV"

var errInvalidArgument = errors.New("invalid argument")

var debug bool

var rootCmd = &cobra.Command{
	Use:           "pmv",
	Short:         "Poor Man's Vault",
	Long:          `A tool for working with 1password secrets`,
	Version:       version,
	SilenceErrors: true,
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		configureLogs()
		cmd.SilenceUsage = true

		return nil
	},
}

// Execute is the main entry point into the cobra application.
func Execute() {
	err := rootCmd.Execute()
	if err == nil {
		return
	}

	lines := []string{err.Error()}

	if errors.Is(err, pmverrors.ErrSetup) {
		lines = append(lines,
			"",
			"It appears that your environment is not setup correctly.",
			"Please review the instructions at:",
			"https://gitlab.com/gitlab-com/gl-infra/pmv/-/blob/main/docs/setup.md",
		)
	}

	var fatalTitleStyle = lipgloss.NewStyle().
		Bold(true).
		Background(lipgloss.Color("#FA4032")).
		Foreground(lipgloss.Color("#FAB12F")).
		Width(82).
		PaddingLeft(2).
		PaddingRight(2)

	fmt.Fprintln(os.Stderr)
	fmt.Fprintln(os.Stderr, fatalTitleStyle.Render("pmv failed ⚠️⚠️"))

	var fatalStyle = lipgloss.NewStyle().
		BorderStyle(lipgloss.OuterHalfBlockBorder()).
		BorderForeground(lipgloss.Color("#FA4032")).
		BorderTop(false).
		BorderLeft(true).
		BorderRight(true).
		BorderBottom(true).
		Width(80).
		PaddingLeft(1).
		PaddingRight(1)

	fmt.Fprintln(os.Stderr, fatalStyle.Render(strings.Join(lines, "\n")))
	fmt.Fprintln(os.Stderr)

	os.Exit(1)
}

func configureLogs() {
	if debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
}

func init() {
	// Setup logging as early as possible,
	// we can adjust it later on args have
	// been parsed.
	consoleWriter := zerolog.ConsoleWriter{
		Out: os.Stderr,
		PartsExclude: []string{
			zerolog.TimestampFieldName,
			zerolog.CallerFieldName,
		},
		FormatLevel: func(i interface{}) string {
			l, ok := i.(string)
			if !ok {
				return "pmv:"
			}

			if l == "info" {
				return "pmv:"
			}

			return "pmv: " + l + ":"
		},
	}
	log.Logger = log.Output(consoleWriter)

	// Default level for this example is info, unless debug flag is present
	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	rootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "Emit debug logs")
}
