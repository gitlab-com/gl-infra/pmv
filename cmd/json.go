package cmd

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

type jsonProcessor struct {
	writer io.Writer
	values map[string]string
}

func (c *jsonProcessor) item(title string, key string, value string) error {
	if strings.HasPrefix(key, "json:") {
		jsonKey := strings.Trim(strings.TrimPrefix(key, "json:"), " ")
		c.values[jsonKey] = value
	}

	return nil
}

func (c *jsonProcessor) done() error {
	jsonString, err := json.Marshal(c.values)
	if err != nil {
		return fmt.Errorf("json marshal failed: %w", err)
	}

	_, err = fmt.Fprint(c.writer, string(jsonString))
	if err != nil {
		return fmt.Errorf("write failed: %w", err)
	}

	return nil
}

var jsonCmd = &cobra.Command{
	Use:          "json",
	ValidArgs:    []string{"tags"},
	Short:        "Generate JSON secrets for vault items",
	Long:         `Generates JSON secrets for vault items`,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		tag := args[0]

		err := run(tag, vault, &jsonProcessor{writer: os.Stdout, values: make(map[string]string)}, defaultModelHandler)
		if err != nil {
			return fmt.Errorf("env failed: %w", err)
		}

		return nil
	},
}

func init() {
	jsonCmd.PersistentFlags().StringVarP(&vault, "vault", "v", "", "Use a specific vault")
	jsonCmd.PersistentFlags().BoolVar(&allowTerminalBypass, "allow-terminal", false, "Allow output to terminal")
	jsonCmd.PersistentFlags().BoolVarP(&allowMultiple, "allow-multiple", "m", false, "Treat more than one item found for tag as ok.")
	jsonCmd.PersistentFlags().BoolVarP(&allowNone, "allow-none", "n", false, "Treat no items found for tag as ok.")

	rootCmd.AddCommand(jsonCmd)
}
