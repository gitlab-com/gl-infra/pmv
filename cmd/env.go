package cmd

import (
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/op"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/terminal"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/totp"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/tokenvalidator"

	log "github.com/rs/zerolog/log"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/aws"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/export"
)

var (
	envPrefix           string
	region              string
	allowTerminalBypass bool
	allowNone           bool
	allowMultiple       bool
	assumeRole          string
	specifiedShell      string
	validateTokens      bool
)

type environmentProcessor struct {
	writer             io.Writer
	envMap             map[string]string
	awsAccessKey       string
	awsSecretAccessKey string

	awsMFATitle    string
	awsMFAArn      string
	totpProviderID string
}

func (c *environmentProcessor) item(title string, key string, value string) error {
	if key == awsMFAArnKey {
		c.awsMFAArn = value
		c.awsMFATitle = title
	}

	if key == "TOTP Provider" {
		c.totpProviderID = value
	}

	if strings.HasPrefix(key, "env:") {
		envKey := strings.TrimPrefix(key, "env:")

		// Store credentials in case we need to perform MFA...
		if envKey == "AWS_ACCESS_KEY" {
			c.awsAccessKey = value
		} else if envKey == "AWS_SECRET_ACCESS_KEY" {
			c.awsSecretAccessKey = value
		}

		c.envMap[envKey] = value
	}

	return nil
}

func (c *environmentProcessor) done() error {
	validateTokensUnneeded := false

	if !skipMFA && c.awsMFAArn != "" {
		err := c.performMFAExchange()
		if err != nil {
			return err
		}

		// Token is now validated since we've performed an MFA with it,
		// no need to perform additional validation.
		validateTokensUnneeded = true
	}

	if assumeRole != "" {
		err := c.performAssumeRole()
		if err != nil {
			return err
		}

		// Token is now validated since we've performed an assume role,
		// no need to perform additional validation.
		validateTokensUnneeded = true
	}

	if validateTokens && !validateTokensUnneeded {
		c.validateToken()
	}

	err := export.Shell(c.envMap, c.calculateUnsetList(), c.writer, envPrefix, specifiedShell)
	if err != nil {
		return fmt.Errorf("export failed: %w", err)
	}

	return nil
}

func (c *environmentProcessor) performAssumeRole() error {
	credentials, err := aws.AssumeRole(c.envMap["AWS_ACCESS_KEY"], c.envMap["AWS_SECRET_ACCESS_KEY"], c.envMap["AWS_SESSION_TOKEN"], assumeRole, region)
	if err != nil {
		return fmt.Errorf("failed to assume role: %w", err)
	}

	c.envMap["AWS_SESSION_TOKEN"] = *credentials.SessionToken
	c.envMap["AWS_ACCESS_KEY"] = *credentials.AccessKeyId
	c.envMap["AWS_ACCESS_KEY_ID"] = *credentials.AccessKeyId
	c.envMap["AWS_SECRET_ACCESS_KEY"] = *credentials.SecretAccessKey

	c.envMap["AWS_SESSION_EXPIRATION"] = credentials.Expiration.UTC().Format(time.RFC3339)

	return nil
}

func (c *environmentProcessor) performMFAExchange() error {
	provider, err := totp.GetProvider(c.totpProviderID)
	if err != nil {
		return fmt.Errorf("totp provider unavailable: %w", err)
	}

	tokenCode, err := provider.Retrieve(c.awsMFAArn)
	if err != nil {
		return fmt.Errorf("failed to fetch token code: %w", err)
	}

	credentials, err := aws.GetSessionToken(c.awsAccessKey, c.awsSecretAccessKey, c.awsMFAArn, tokenCode, region)
	if err != nil {
		return fmt.Errorf("failed to perform MFA using the supplied code: %w", err)
	}

	c.envMap["AWS_SESSION_TOKEN"] = *credentials.SessionToken
	c.envMap["AWS_ACCESS_KEY"] = *credentials.AccessKeyId
	c.envMap["AWS_ACCESS_KEY_ID"] = *credentials.AccessKeyId
	c.envMap["AWS_SECRET_ACCESS_KEY"] = *credentials.SecretAccessKey

	c.envMap["AWS_SESSION_EXPIRATION"] = credentials.Expiration.UTC().Format(time.RFC3339)

	return nil
}

func (c *environmentProcessor) calculateUnsetList() []string {
	// For AWS, if AWS_SESSION_TOKEN is not set, but remains from a previous session,
	// we should unclear it
	if c.envMapContains("AWS_ACCESS_KEY", "AWS_ACCESS_KEY_ID", "AWS_SECRET_ACCESS_KEY") {
		return []string{"AWS_SESSION_TOKEN"}
	}

	return []string{}
}

func (c *environmentProcessor) envMapContains(keys ...string) bool {
	for _, k := range keys {
		_, ok := c.envMap[k]
		if ok {
			return true
		}
	}

	return false
}

func (c *environmentProcessor) validateToken() {
	err := tokenvalidator.ValidateToken(c.envMap)
	if err != nil {
		log.Fatal().
			Msg("Token validation failed. Token is invalid.")
	}
}

var envCmd = &cobra.Command{
	Use:          "env",
	ValidArgs:    []string{"tags"},
	Short:        "Generate environment variables for vault items",
	Long:         `Generates environment variables, exported for bash`,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		tag := args[0]

		if terminal.CheckForTerminal(allowTerminalBypass) != nil {
			os.Exit(3)
		}

		_, signinEnv, err := op.EnsureSignin()
		if err != nil {
			return fmt.Errorf("unable to determine op signin status: %w", err)
		}

		if signinEnv == nil {
			signinEnv = make(map[string]string)
		}

		err = run(tag, vault, &environmentProcessor{
			writer: os.Stdout,
			envMap: signinEnv,
		}, defaultModelHandler)
		if err != nil {
			return fmt.Errorf("env failed: %w", err)
		}

		return nil
	},
}

func init() {
	envCmd.PersistentFlags().StringVarP(&vault, "vault", "v", "", "Use a specific vault")
	envCmd.PersistentFlags().StringVarP(&envPrefix, "prefix", "p", "", "Prefix to add to environment variables")
	envCmd.PersistentFlags().StringVarP(&region, "region", "r", "", "AWS region to use for validating MFA (typically only required in gov-cloud).")
	envCmd.PersistentFlags().BoolVar(&allowTerminalBypass, "allow-terminal", false, "Allow output to terminal")
	envCmd.PersistentFlags().BoolVarP(&allowMultiple, "allow-multiple", "m", false, "Treat more than one item found for tag as ok.")
	envCmd.PersistentFlags().BoolVarP(&allowNone, "allow-none", "n", false, "Treat no items found for tag as ok.")
	envCmd.PersistentFlags().BoolVar(&skipMFA, "skip-mfa", false,
		"Don't create a temporary session using MFA, provide the underlying permanent credentials only. "+
			"Dangerous as this option circumvents MFA and could expose underlying credentials.")
	envCmd.PersistentFlags().StringVarP(&assumeRole, "assume-role", "A", "", "AWS role to assume after obtaining credentials.")
	envCmd.PersistentFlags().StringVarP(&specifiedShell, "shell", "", "", "Create commands for a specific shell, defaults to $SHELL")
	envCmd.PersistentFlags().BoolVar(&validateTokens, "validate-tokens", false,
		"Use best-effort attempt to validate the tokens before exporting them. Only supported for GitLab and AWS tokens.")

	rootCmd.AddCommand(envCmd)
}
