package cmd

import (
	"fmt"
	"os"
	"time"

	log "github.com/rs/zerolog/log"

	"github.com/spf13/cobra"

	"gitlab.com/gitlab-com/gl-infra/pmv/internal/export"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/gitlab"
	"gitlab.com/gitlab-com/gl-infra/pmv/internal/terminal"
)

var gitlabEnvCmd = &cobra.Command{
	Use:          "env",
	ValidArgs:    []string{},
	Short:        "Performs a GitLab OAuth2 authentication and exports the GITLAB_TOKEN to the environment",
	Long:         `Performs a GitLab OAuth2 authentication and exports the GITLAB_TOKEN to the environment`,
	Args:         cobra.ExactArgs(0),
	SilenceUsage: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		if terminal.CheckForTerminal(allowTerminalBypass) != nil {
			os.Exit(3)
		}

		token, err := gitlab.PerformOAuth2Authentication(cmd.Context())
		if err != nil {
			return fmt.Errorf("gitlab oauth2 failed: %w", err)
		}

		envMap := map[string]string{
			"GITLAB_OAUTH_TOKEN":            token.AccessToken,
			"GITLAB_OAUTH_REFRESH_TOKEN":    token.RefreshToken,
			"GITLAB_OAUTH_TOKEN_TYPE":       token.TokenType,
			"GITLAB_OAUTH_TOKEN_EXPIRATION": token.Expiry.UTC().Format(time.RFC3339),
		}

		err = export.Shell(envMap, nil, cmd.OutOrStdout(), envPrefix, specifiedShell)
		if err != nil {
			return fmt.Errorf("failed to write exported: %w", err)
		}

		log.Info().
			Msg("🔓 Complete, authentication configured...")

		return nil
	},
}

func init() {
	gitlabCmd.AddCommand(gitlabEnvCmd)
	gitlabEnvCmd.PersistentFlags().BoolVar(&allowTerminalBypass, "allow-terminal", false, "Allow output to terminal")
	gitlabEnvCmd.PersistentFlags().StringVarP(&envPrefix, "prefix", "p", "", "Prefix to add to environment variables")
	gitlabEnvCmd.PersistentFlags().StringVarP(&specifiedShell, "shell", "", "", "Create commands for a specific shell, defaults to $SHELL")
}
