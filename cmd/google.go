package cmd

import (
	"github.com/spf13/cobra"
)

var organization string

var googleCmd = &cobra.Command{
	Use:       "google",
	ValidArgs: []string{},
	Short:     "Perform Google login",
	Long:      `Perform Google login`,
}

func init() {
	rootCmd.AddCommand(googleCmd)
	googleCmd.PersistentFlags().StringVarP(&organization, "organization", "O", "gitlab.com",
		"Google Organization to authenticate against. Supports gitlab.com, gitlab-private.org")
}
