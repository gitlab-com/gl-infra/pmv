package cmd

import (
	"github.com/spf13/cobra"
)

var tags string
var title string
var description string
var deleteTaggedItems bool
var skipMFA bool

const opLoginCategory = "LOGIN"

var captureCmd = &cobra.Command{
	Use:       "capture",
	ValidArgs: []string{"tags"},
	Short:     "Capture credentials in 1password",
	Long:      `Capture credentials in 1password`,
}

func init() {
	captureCmd.PersistentFlags().StringVarP(&vault, "vault", "v", "", "Use a specific vault")
	captureCmd.PersistentFlags().StringVarP(&tags, "tags", "T", "", "Comma-delimited tags for the item")
	captureCmd.PersistentFlags().StringVarP(&title, "title", "t", "", "Title for the item")
	captureCmd.PersistentFlags().StringVar(&description, "description", "", "Description, used for generating a title")
	captureCmd.PersistentFlags().BoolVar(&deleteTaggedItems, "delete-tagged-items", false, "Delete any previous items with the given tag")
	captureCmd.PersistentFlags().BoolVar(&skipMFA, "skip-mfa", false, "Skip MFA enrolment")
	rootCmd.AddCommand(captureCmd)
}
