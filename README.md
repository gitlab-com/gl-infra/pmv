# PMV

PMV is a tiny utility for working with the [1password CLI](https://developer.1password.com/docs/cli/get-started#usage).

Note: PMV requires **1password CLI v2**. If you need to use `pmv` with 1password-cli v1, please use [pmv v2 or older](https://gitlab.com/gitlab-com/gl-infra/pmv/-/releases/v2.4.21).

## Installing and Running PMV

1. Please follow the [**setup documentation](./docs/setup.md) to get started.

## Using PMV

1. [AWS Multi-Factor Authentication setup using Yubikey Oath and PMV](./docs/aws-mfa.md).

## 1password CLI Caveats

### Macos

On Macos, the 1password CLI uses the Secure Enclave. For this reason, it's recommended to not use `asdf` or other version management tools to install the 1password CLI. This is because they will likely not install `1password-cli` in `/usr/local/bin`, which is required for usage with the Secure Enclave.

It's recommended that you either uninstall the asdf `1password-cli` plugin, or use "system" as the version in your `.tool-versions`.

Then, install 1password-cli manually, following the instructions at <https://developer.1password.com/docs/cli/get-started#install>.

## `pmv` Commands

### `env`

Generates a set of environment export variables from a 1password tag.

1. Login to 1password, eg `eval $(op signin --account gitlab)`
1. Create an item in 1password and configure it with a unique tag. Note that slashes (`/`) in tags will be shown hierarchically in the 1password UI, which can be useful for categorization.
1. Each field with a `env:` prefix will be emitted as an export. For example `env:GITLAB_TOKEN=xyz` will generate `export GITLAB_TOKEN=xyz`.
1. Use `eval $(pmv env Tag)` to export the environment variables into the current shell.
1. To add a prefix to each item, use the optional `--prefix PREFIX_` argument.
1. By default `pmv env` will create a temporary session if an MFA is registered. This behavior can be disabled with the `--skip-mfa` flag. **Warning**: this option is dangerous as it can circumvent MFA and could expose underlying credentials. Use with caution.
1. Use the `--assume-role` parameter to assume another role. This parameter will accept a role name, or a role ARN.
1. Use the `--validate-tokens` parameter to validate a token. Currently supports GitLab and AWS tokens. Other tokens will pass.

#### Usage

```console
$ # Before using pmv, log using 1password client
$ eval $(op signin --account gitlab)
$ # Sample usage of `pmv env`
$ pmv env ProductName/Env:Test
export SECRET=abc
export OTHER_SECRET=xyz
$ # More useful usage, exports variables to shell
$ eval $(pmv env ProductName/Env:Test)
$ # Secrets are now loaded into the environment
$ # Add a prefix to the items
$ pmv env ProductName/Env:Test --prefix HUB_
export HUB_SECRET=abc
export HUB_OTHER_SECRET=xyz
```

The above usage can be combined with [direnv](https://direnv.net) to automatically load and unload environment variables from 1Password. While this can be achieved using `direnv` alone, a single call to 1Password is a lot faster when loading multiple environment variables.

Add the following to a file called `.envrc` to any directory and these environment variables will be loaded at that directory and any child directories.

```bash
#!/bin/bash

eval "$(pmv env ProductName/Env:Test)"
```

### `json`

Generates a secrets blob in JSON

1. Login to 1password, eg `eval $(op signin --account gitlab)`
1. Create an item in 1password and configure it with a unique tag. Note that slashes (`/`) in tags will be shown hierarchically in the 1password UI, which can be useful for categorization.
1. Each field with a `json:` prefix will be emitted as an export. For example `json:secret=xyz` will generate `{"secret": "xyz"}`.
1. Use `pmv env Tag > secrets.json` to write the secrets to a file.

#### Usage

```console
$ # Before using pmv, log using 1password client
$ eval $(op signin --account gitlab)
$ # Sample usage of `pmv json`
$ pmv json ProductName/Env:Test > secrets.json
```

### `capture aws`

Captures credentials to AWS for use in Environments. Will validate the credentials before saving them.

1. Login to 1password, eg `eval $(op signin --account gitlab)`
1. Run: `pmv capture aws --description "My Production Credentials" --tags "MyTag,MyOtherTag" --vault "DefaultsToPersonal" --delete-tagged-items`
1. You can set a title for the item with `--title`, but if you choose `--description`, a title, including details such as AWS Account ID, Account Alias, Username and your chosen Description will be generated.
1. ⚠️ Note: using `--delete-tagged-items` will remove any others items with the given tags from the vault.

#### Setting up Virtual MFA

`pmv capture aws` will, by default, setup a Virtual MFA device if one is not setup for the account.

The AWS API does not support U2F (eg Yubikey) MFA devices,
but PMV will facilitate access to TOTP codes stored on Yubikey using the [Oath application](https://developers.yubico.com/OATH/).

MFA setup is complicated due to recently introduced Amazon security restrictions, but [there is a guide to setting it up](./docs/aws-mfa.md).

The serial number/arn of the Virtual MFA will then be saved along with the captured authentication details in the newly created 1password item.

Note, MFA setup can be disabled with the `--skip-mfa` argument.

#### Usage

```console
$ # Before using pmv, log using 1password client
$ eval $(op signin --account gitlab)
$ # Sample usage of `pmv capture aws`
$ pmv capture aws --description "My Production Credentials" --tags "MyTag"  --delete-tagged-items
pmv: 🔓 Enter AWS_ACCESS_KEY: AKIA474747474747
pmv: 🔓 Enter AWS_SECRET_ACCESS_KEY:
pmv: 🔓 Thanks. Verifying the credentials with AWS...
pmv: ✅ Verification passed. Greetings! 👋 username=andrew-c42c5285
pmv: 📲 No  MFA token found: let's set you up with a new one...
pmv: 📲 Review the instructions at URL
pmv: 🔓 Enter MFA Secret Code:
pmv: 📲 Confirmation Code #1 code_1=111222
pmv: 📲 Confirmation Code #2 code_2=333444
pmv: 📲 Return to your browser, and enter these confirmation codes in the AWS Console New Virtual MFA confirmation dialog code_1=111222 code_2=333444
pmv: 📲 Discovered new device serial_number=arn:aws:iam::452919332838:mfa/andrewn
pmv: 📲 Ok. Let's confirm that your TOTP token is working.
Touch your YubiKey...
pmv: 🐣 Created item link=https://start.1password.com/open/i?a=LKATQYUATRBRDHRRABEBH4RJ5Y&v=5albo45xgknr5k4xm4dlq3whk4&i=ivmzeodbbw2btowlf33fkxh57m&h=gitlab.1password.com
```

### Performing Multi-factor Authentication using the Virtual MFA

After the MFA has been setup with `pmv capture aws`, the `pmv env` command will automatically perform multi-factor authentication using the configuration virtual MFA device.

```console
$ # Log into the account, with Multi-Factor Authentication
$ eval $(pmv env MyTag)
pmv: 🔓 Fetching 1password items with tag MyTag...
pmv: 🧩 Loaded: `AWS Access: andrew-api: Account ID 11111111111`
pmv: 📲 Use your Authenticator app to provide a token code for `AWS Access: andrew-api: Account ID 11111111111`:
token: 111111
pmv: 🔓 Complete, loaded items...
$ # Notice that the AWS_ACCESS_KEY now starts with an `AS..` instead of the `AK`. This indicates a temporary session key is in use.
$ set | grep AWS_
AWS_ACCESS_KEY=AS1111111111111111
AWS_ACCESS_KEY_ID=AS1111111111111111
AWS_SECRET_ACCESS_KEY=K11111...
AWS_SESSION_TOKEN=F1111...
...
$ # Check the session expiry time stored in AWS_SESSION_EXPIRATION. The 12 hour default is used.
$ echo $AWS_SESSION_EXPIRATION
2022-08-30T08:09:05Z
```

### `capture gitlab`

Captures credentials for GitLab token usage, into an environment variable, `GITLAB_TOKEN`. Will validate the credentials before saving them.

1. Login to 1password, eg `eval $(op signin --account gitlab)`
1. Run: `pmv capture gitlab --description "My GitLab Access Token" --tags "MyGitLabTag" --vault "DefaultsToPersonal" --delete-tagged-items`
1. You can set a title for the item with `--title`, but if you choose `--description`, a title, including details such as Username, GitLab Instance and your chosen Description will be generated.
1. By default, the token will be tested against GitLab.com, but this can be changed with `--gitlab-instance https://gitlab.example.com`
1. ⚠️ Note: using `--delete-tagged-items` will remove any others items with the given tags from the vault.

### `capture aws-secret`

Captures credentials from AWS SecretsManager, into an 1password item.

1. The AWS client uses the `AWS_*` environment variables to initialize AWS communications. `pmv` will fail to fetch AWS SecretsManager items unless these are properly configured. This can be validated by running `aws sts get-caller-identity`.
1. Login to 1password, eg `eval $(op signin --account gitlab)`
1. Run: `pmv capture aws-secret --secret-id "gitlab/dedicated/env/pagerduty_service_key" --title "Pagerduty Service Key" --tags PagerdutyServiceKey --delete-tagged-items`
1. The `--title` and `--secret-id` arguments are both required.
1. ⚠️ Note: using `--delete-tagged-items` will remove any others items with the given tags from the vault.

### `rotate aws <tag>`

This allows a AWS Secrets Manager secret, previously created using [`capture aws`](#capture-aws) to be automatically rotated.

```console
$ pmv rotate aws "MyTag"
pmv: 🔓 Fetching 1password items with tag MyTag...
pmv: 🧩 Loaded: `MyTag`
pmv: 🔓 Complete, loaded items...
pmv: 🔑  Creating new access key in AWS...
pmv: 🐣  Creating new 1password item...
pmv: ❌  Deleting old key in AWS...
pmv: ✨  Created item: https://start.1password.com/open/i?a=xx&v=xx&i=xx&h=gitlab.1password.com
pmv: 🗑️  Deleting old 1password item...
pmv: 🚀 Rotation complete...
```

1. By default, the old key will be deleted, but you can chose to inactivate it instead. This can be done using the `--inactivate-old-key` argument.
1. By default, AWS accounts will allow a maximum of two access keys. If you have two active, you will need to delete one of your keys first, since the rotation will create the new key before removing the old one.

### `rotate gitlab <tag>`

Uses the GitLab Token rotation API to rotate a token.
By default, the new token will have an expiry 1 day less than 1 year into the future.

```console
$ pmv rotate gitlab "MyTag"
pmv: 🔓 Fetching 1password items tag=Dedicated/Sandbox/GitLab.com/PAT:Personal
pmv: 🧩 Loaded item title="GitLab Access Token: GitLab.com PAT for Sandbox Access (api): https://gitlab.com: andrewn"
pmv: 🔓 Complete, loaded items...
pmv: 🔑  Rotating token in GitLab...
pmv: 🔑  New token expiry date... expires_at=2025-05-16
pmv: 🐣  Creating new 1password item...
pmv: 🐣 Created item link=https://start.1password.com/open/...
pmv: 🗑️  Deleting old 1password item.
pmv: 🚀 Rotation complete...
```

1. Unfortunately this can only be used for Personal Access Tokens at present, until https://gitlab.com/gitlab-org/gitlab/-/issues/462419 is resolved.
1. Use the `--expires` argument to set an expiry. This can either be a date in `YYYY-MM-DD` format, or a duration, eg: `30d`.
1. If your token does not have the `api` scope, it will be possible to rotate it using another token for the same user specifying the `--with` parameter.

### `sso env`

This performs an SSO sign-on into a AWS account using an OIDC exchange.

```console
$ # Use OIDC to authenticate with AWS Identity Center SSO
$ eval $(pmv sso env --start-url https://d-9067908776.awsapps.com/start --account-id 787839063675 --role-name AdministratorAccess)
$ aws sts get-caller-identity
{
    "UserId": "XXXXXXXXXXXXXXXXXXXXXX:andrew@gitlab.com",
    "Account": "787839063675",
    "Arn": "arn:aws:sts::787839063675:assumed-role/AWSReservedSSO_AdministratorAccess_XXXXXXXX/andrew@gitlab.com"
}
```

### `gitlab env`

This performs an [OAuth2 PKCE exchange](https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-with-proof-key-for-code-exchange-pkce) against gitlab.com and exports a temporary access token to the environment.

```console
$ # Use OAuth2 to authenticate with GitLab.com
$ eval $(pmv gitlab env)
$ # Pass the OAuth2 token through the Authorization header
$ curl --header "Authorization: Bearer $GITLAB_OAUTH_TOKEN" https://gitlab.com/api/v4/user
{"id":895869,"username":"andrewn" ... }
$ # Alternatively, use the token with access_token request parameter
$ curl "https://gitlab.com/api/v4/user?access_token=$GITLAB_OAUTH_TOKEN"
{"id":895869,"username":"andrewn" ... }
```

### `google env`

Performs an [OAuth2 PKCE exchange](https://developers.google.com/identity/protocols/oauth2/native-app) against Google, and exports a temporary access token to the environment.

Unlike the default CLI, this command does not write credentials to the file `~/.config/gcloud/application_default_credentials.json`.

```console
$ # Use OAuth2 to authenticate with Google
$ eval $(pmv google env)
$ gcloud projects list
PROJECT_ID                      NAME                            PROJECT_NUMBER
aaa-111                         aaa-111                         123456789012
...
```

#### Organization Support

By default `pmv google env` will use the `gitlab.com` Google Organization,
but other GitLab-controlled organizations can be selected using the `--organization` command-line argument.

Currently supported organizations are:

1. `gitlab.com`: `pmv google env --organization "gitlab.com"` GitLab Main Organization
1. `gitlab-private.org`: `pmv google env --organization "gitlab-private.org"` GitLab Dedicated Dev Organization

Additional organizations can be registered as needed.
