# setup

PMV can be installed via [GitLab Releases](https://gitlab.com/gitlab-com/gl-infra/pmv/-/releases).

Alternatively, it can be installed via an [`asdf` plugin](https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra).

## Required Software

### 1Password CLI

PMV requires 1Password CLI to be installed on your computer.
Follow the instructions at
<https://developer.1password.com/docs/cli/get-started/> to set 1password-cli up on your workstation.

**For Macos users, 1password-cli can be installed using Homebrew: `brew install 1password-cli`.**

### Yubikey

All GitLab employees need to have a Yubikey as a multi-factor authentication device.
In order to interact with your Yubikey, please install the Yubikey Manager CLI, `ykman`.

Installation instructions for `ykman` are available at https://docs.yubico.com/software/yubikey/tools/ykman/Install_ykman.html#download-label.

**For Macos users, `ykman` can be installed using Homebrew: `brew install ykman`.**
