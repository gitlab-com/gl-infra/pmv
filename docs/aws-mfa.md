# AWS MFA Setup

Unfortunately AWS MFA configuration is unnecessarily complicated.
After the <https://www.mwrcybersec.com/when-mfa-becomes-sfa> vulnerability was discovered,
AWS prevented MFA changes via the API except when the user is already authenticated with an MFA, or the user

This, coupled with the fact that the [AWS API does not support U2F MFA](https://github.com/aws/aws-cli/issues/3607),
effectively means that the  AWS API can no longer be used to setup Virtual TOTP MFA devices,
as required for API access.

This means MFA devices must be created through the AWS Console.

PMV will guide the user through the process of doing this.

----------------------------------------------------------

## Guide

### Prerequisites

1. Ensure that PMV is ready [as per the setup guide](./setup.md).
2. Confirm that the version of `pmv` installed >`v3.20.1`

**Note** The `pmv` version needs to be correct outside of the Dedicated project directories as that version will be used by [direnv](https://direnv.net/) to fetch credentials.

```bash
cd ~
pmv --version # Must be > v3.20.1
```

### Step 2: Log into the AWS Console

1. Visit the [**AWS Console Security Credentials Page**](https://us-east-1.console.aws.amazon.com/iam/home?region=us-east-1#/security_credentials?section=IAM_credentials)
   and create **a new Access Key** for use on your workstation.

   ![Security Credentials](./images/aws-mfa-001.png)

### Step 1: run Capture AWS

```console
$ # capture a new credential, and setup MFA
$ pmv capture aws -t 'MyAWSCredential'
pmv: 🔓 Enter AWS_ACCESS_KEY: AKIAWHATUP
pmv: 🔓 Enter AWS_SECRET_ACCESS_KEY:
pmv: 🔓 Thanks. Verifying the credentials with AWS...
pmv: ✅ Verification passed. Greetings! 👋 username=andrew-c42c5285
pmv: 📲 No MFA token found: let's set you up with a new one...
pmv: 📲 Review the instructions at https://gitlab.com/gitlab-com/gl-infra/pmv/-/blob/main/docs/aws-mfa.md
pmv: 🔓 Enter MFA Secret Code:
```

At this point, PMV will be waiting for an MFA Secret Code.
We need to obtain this in the AWS Console,
so switch back to your browser.

### Step 2: Create a New Virtual MFA and obtain an MFA Secret Code

1. Recommended: open a second browser tab, so as to preserve your Access Key in the first browser tag, in case the process fails.
1. Visit the [**IAM Credentials**](https://us-east-1.console.aws.amazon.com/iam/home?region=us-east-1#/security_credentials?section=IAM_credentials)
   page and **Assign an MFA device**.

   ![Assign an MFA device](./images/aws-mfa-002.png)

1. **Device Name** must be unique across the entire AWS Account, so use your username, or prefix the device name with your username.
1. Choose **Authenticator App**.

   ![New MFA](./images/aws-mfa-003.png)

1. Click **Next**
1. You will now be presented with the **Device Setup** screen.
   There is no need to show QR Code.
   Click the **Show secret key** link, and copy the key into your pastebuffer.

   ![Show secret key](./images/aws-mfa-004.png)

1. Return to your terminal, and **paste the secret key into the `Enter MFA Secret Code` prompt**.
   Then press enter.

### Step 3: Wait for MFA Confirmation Codes from PMV

PMV will generate two confirmation codes.
This can take up to 30 seconds as the codes are time based, so be patient.

```console
pmv: 🔓 Enter MFA Secret Code:
pmv: 📲 Confirmation Code #1 code_1=123456
pmv: 📲 Confirmation Code #2 code_2=654321
pmv: 📲 Return to your browser, and enter these confirmation codes in the AWS Console New Virtual MFA confirmation dialog code_1=123456 code_2=654321
```

### Step 4: Complete the MFA Device Setup in your Browser

1. Copy both of the codes, (eg, `123456` and `654321`) from your terminal window,
   into the AWS Console MFA Device Setup dialog.
1. Click **Add MFA Device** to finish the setup process.
1. Return to your terminal to complete the setup.

### Step 5: Finalization and Confirmation

1. PMV will poll your AWS account and detect the newly enabled key.
   Once it has detected it, it will configure your TOTP in Yubikey
   and your Access Key and Secret in a 1Password item.

    ```console
    pmv: 📲 Discovered new device serial_number=arn:aws:iam::111111111111:mfa/andrewn
    ```

1. Finally, to validate that everything is working, PMV will perform a
   two factor authentication exchange against your AWS account.
   Since the TOTP is now stored in Yubikey, and PMV requires it,
   your Yubikey will flash, indicating that it needs attention.
   Touch the Yubikey to grant PMV access to the TOTP.

    ```console
    pmv: 📲 Ok. Let's confirm that your TOTP token is working.
    Touch your YubiKey...
    pmv: 🐣 Created item link=https://start.1password.com/open/i?a=213123123123123&v=123123123123123&i=232112312312&h=gitlab.1password.com
    ```

1. You are now ready to go! Congratulations.
